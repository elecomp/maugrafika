/*
Navicat MySQL Data Transfer

Source Server         : Server Local
Source Server Version : 50532
Source Host           : 127.0.0.1:3306
Source Database       : db_onlineshop

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2018-09-27 11:12:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for data_bank
-- ----------------------------
DROP TABLE IF EXISTS `data_bank`;
CREATE TABLE `data_bank` (
  `id_data` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_bank` varchar(255) NOT NULL,
  `atas_nama_bank` varchar(255) NOT NULL,
  `no_rekening` varchar(255) NOT NULL,
  PRIMARY KEY (`id_data`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of data_bank
-- ----------------------------
INSERT INTO `data_bank` VALUES ('1', 'Mandiri', 'jeverashop', '32523523');
INSERT INTO `data_bank` VALUES ('2', 'BRI', 'jeverashop', '52352');

-- ----------------------------
-- Table structure for data_refund
-- ----------------------------
DROP TABLE IF EXISTS `data_refund`;
CREATE TABLE `data_refund` (
  `id_refund` int(11) NOT NULL AUTO_INCREMENT,
  `id_detail_order` int(11) NOT NULL,
  `no_rekening` varchar(50) NOT NULL,
  `ATM` varchar(50) NOT NULL,
  `nama_penerima` varchar(50) NOT NULL,
  `status_kirim` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_refund`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of data_refund
-- ----------------------------

-- ----------------------------
-- Table structure for detail_order
-- ----------------------------
DROP TABLE IF EXISTS `detail_order`;
CREATE TABLE `detail_order` (
  `id_detail_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` varchar(20) NOT NULL,
  `id_penjual` varchar(50) NOT NULL,
  `id_ongkir_pembeli` int(11) NOT NULL,
  `id_produk` varchar(15) NOT NULL,
  `jumlah_produk` int(11) NOT NULL,
  `berat_produk` double NOT NULL,
  `harga` double NOT NULL,
  `harga_pajak` double NOT NULL,
  `subtotal` double NOT NULL,
  `subtotal_pajak` double NOT NULL,
  `no_resi` varchar(50) NOT NULL,
  `pembayaran` double NOT NULL,
  `tagihan` double NOT NULL,
  `status_kirim` int(1) NOT NULL,
  `status_detail_komplain` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_detail_order`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_order
-- ----------------------------
INSERT INTO `detail_order` VALUES ('1', 'T170417001', 'wildan', '0', '1701170003', '8', '96', '1200', '1080', '9600', '8640', '', '0', '0', '0', '0');
INSERT INTO `detail_order` VALUES ('2', 'T170517001', 'wildanafif', '0', '1705170001', '2', '18', '200000', '160000', '400000', '320000', '75888', '320000', '320000', '1', '0');
INSERT INTO `detail_order` VALUES ('3', 'T170520001', 'afif', '0', '1705200001', '2', '20', '900000', '855000', '1800000', '1710000', '324321', '1710000', '1710000', '1', '0');
INSERT INTO `detail_order` VALUES ('4', 'T180920001', 'yayanraw', '0', '126', '1', '100', '40000', '36000', '40000', '36000', '', '0', '0', '0', '0');
INSERT INTO `detail_order` VALUES ('5', 'T180922001', 'SMKN4Malang', '0', '123', '2', '200', '40000', '36000', '80000', '72000', '', '0', '0', '0', '0');
INSERT INTO `detail_order` VALUES ('6', 'T180922002', 'SMKN4Malang', '0', '121', '1', '100', '22000', '19800', '22000', '19800', '', '0', '0', '0', '0');
INSERT INTO `detail_order` VALUES ('7', 'T180922003', 'SMKN4Malang', '0', '121', '1', '100', '22000', '19800', '22000', '19800', '', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for detail_pengiriman
-- ----------------------------
DROP TABLE IF EXISTS `detail_pengiriman`;
CREATE TABLE `detail_pengiriman` (
  `id_detail_pengiriman` int(11) NOT NULL AUTO_INCREMENT,
  `id_detail_order` int(11) NOT NULL,
  `tanggal_konfirmasi` date NOT NULL,
  `tanggal_kirim` date DEFAULT NULL,
  `status_kadaluarsa` int(1) NOT NULL,
  PRIMARY KEY (`id_detail_pengiriman`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_pengiriman
-- ----------------------------
INSERT INTO `detail_pengiriman` VALUES ('1', '2', '2017-05-17', '2017-05-17', '0');
INSERT INTO `detail_pengiriman` VALUES ('2', '3', '2017-05-20', '2017-05-20', '0');

-- ----------------------------
-- Table structure for kategori_produk
-- ----------------------------
DROP TABLE IF EXISTS `kategori_produk`;
CREATE TABLE `kategori_produk` (
  `id_kategori_produk` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori_produk` varchar(100) NOT NULL,
  `aktif_kategori_produk` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `title_kategori_produk` varchar(50) NOT NULL,
  `meta_description_kategori_produk` varchar(100) NOT NULL,
  `meta_keywords_kategori_produk` varchar(100) NOT NULL,
  PRIMARY KEY (`id_kategori_produk`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kategori_produk
-- ----------------------------
INSERT INTO `kategori_produk` VALUES ('1', 'Rendang', '1', '4', 'Rendang', 'Rendang', 'Rendang');
INSERT INTO `kategori_produk` VALUES ('2', 'Rendang Seasoning', '1', '5', 'Rendang Seasoning', 'Rendang Seasoning', 'Rendang Seasoning');

-- ----------------------------
-- Table structure for keranjang_belanja
-- ----------------------------
DROP TABLE IF EXISTS `keranjang_belanja`;
CREATE TABLE `keranjang_belanja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_keranjang_belanja` varchar(60) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `harga_produk` double NOT NULL,
  `jumlah_produk` int(11) NOT NULL,
  `subtotal_belanja` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of keranjang_belanja
-- ----------------------------

-- ----------------------------
-- Table structure for klaim_detail_order
-- ----------------------------
DROP TABLE IF EXISTS `klaim_detail_order`;
CREATE TABLE `klaim_detail_order` (
  `id_detail_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` varchar(20) NOT NULL,
  `id_penjual` varchar(50) NOT NULL,
  `id_produk` varchar(15) NOT NULL,
  `jumlah_produk` int(11) NOT NULL,
  `berat_produk` double NOT NULL,
  `harga` double NOT NULL,
  `harga_pajak` double NOT NULL,
  `subtotal` double NOT NULL,
  `subtotal_pajak` double NOT NULL,
  `no_resi` varchar(50) NOT NULL,
  `pembayaran` double NOT NULL,
  `tagihan` double NOT NULL,
  `status_kirim` int(1) NOT NULL,
  `keterangan` text NOT NULL,
  `id_ongkir_pembeli` varchar(255) NOT NULL,
  `status_detail_komplain` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of klaim_detail_order
-- ----------------------------

-- ----------------------------
-- Table structure for komplain_barang
-- ----------------------------
DROP TABLE IF EXISTS `komplain_barang`;
CREATE TABLE `komplain_barang` (
  `id_komplain` int(11) NOT NULL AUTO_INCREMENT,
  `id_detail_order` varchar(50) NOT NULL,
  `id_penjual` varchar(50) NOT NULL,
  `pesan_komplain` text NOT NULL,
  `bukti_komplain` varchar(50) NOT NULL,
  `jumlah_produk_komplain` int(11) NOT NULL,
  `jenis_komplain` varchar(255) NOT NULL,
  `status_komplain` varchar(50) NOT NULL DEFAULT 'Belum Ditangani',
  `tgl_komplain` date NOT NULL,
  `status_dana_kembali` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_komplain`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of komplain_barang
-- ----------------------------

-- ----------------------------
-- Table structure for konfirmasi_bayar
-- ----------------------------
DROP TABLE IF EXISTS `konfirmasi_bayar`;
CREATE TABLE `konfirmasi_bayar` (
  `id_konfirmasi` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_konfirmasi` date NOT NULL,
  `id_order` varchar(255) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `bank_bayar` varchar(20) NOT NULL,
  `rekening_bayar` varchar(30) NOT NULL,
  `nama_bayar` varchar(30) NOT NULL,
  `foto` text NOT NULL,
  PRIMARY KEY (`id_konfirmasi`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of konfirmasi_bayar
-- ----------------------------
INSERT INTO `konfirmasi_bayar` VALUES ('1', '2017-05-17', 'T170517001', '407000', 'Mandiri', '21312', 'RObi', '');
INSERT INTO `konfirmasi_bayar` VALUES ('2', '2017-05-20', 'T170520001', '1804000', 'Mandiri', '312312', 'Wildan', '');
INSERT INTO `konfirmasi_bayar` VALUES ('3', '2018-09-22', 'T180922001', '1000000', 'bca', '000', 'edwin', 'edwin.');
INSERT INTO `konfirmasi_bayar` VALUES ('4', '2018-09-22', 'T180922001', '11', 'bca', '111', 'ada', '111.jpg');
INSERT INTO `konfirmasi_bayar` VALUES ('5', '2018-09-22', 'T180922003', '180000', 'bcd', '111', 'edwin', '111.jpg');

-- ----------------------------
-- Table structure for konfirmasi_penerimaan
-- ----------------------------
DROP TABLE IF EXISTS `konfirmasi_penerimaan`;
CREATE TABLE `konfirmasi_penerimaan` (
  `id_penerimaan` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `id_penjual` varchar(50) NOT NULL,
  `tanggal_penerimaan` date NOT NULL,
  `foto_bukti` varchar(100) NOT NULL,
  PRIMARY KEY (`id_penerimaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of konfirmasi_penerimaan
-- ----------------------------

-- ----------------------------
-- Table structure for konfirmasi_pengembalian_produk
-- ----------------------------
DROP TABLE IF EXISTS `konfirmasi_pengembalian_produk`;
CREATE TABLE `konfirmasi_pengembalian_produk` (
  `id_konfirmasi_pengembalian_produk` int(11) NOT NULL AUTO_INCREMENT,
  `id_komplain_barang` int(11) NOT NULL,
  `id_detail_order` int(11) NOT NULL,
  `id_order` varchar(255) NOT NULL,
  `no_resi_pengembalian` varchar(255) NOT NULL,
  `no_rek` varchar(255) NOT NULL,
  `nama_rek` varchar(255) NOT NULL,
  `jenis_bank` varchar(255) NOT NULL,
  `status_sampai` int(11) NOT NULL DEFAULT '0',
  `no_resi_ganti` varchar(255) NOT NULL,
  PRIMARY KEY (`id_konfirmasi_pengembalian_produk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of konfirmasi_pengembalian_produk
-- ----------------------------

-- ----------------------------
-- Table structure for konten
-- ----------------------------
DROP TABLE IF EXISTS `konten`;
CREATE TABLE `konten` (
  `id_konten` int(11) NOT NULL,
  `tentang` text NOT NULL,
  `aturan` text NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `Email` varchar(20) NOT NULL,
  `panduan` text NOT NULL,
  PRIMARY KEY (`id_konten`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of konten
-- ----------------------------
INSERT INTO `konten` VALUES ('1', '<article helvetica=\"\" style=\"box-sizing: border-box; color: rgb(128, 128, 128); font-family: \">\n	<p style=\"box-sizing: border-box; margin: 0px 0px 15px;\">\n		Jevera merupakan salah satu&nbsp;<strong style=\"box-sizing: border-box;\"><em style=\"box-sizing: border-box;\">online marketplace</em>&nbsp;terkemuka di Indonesia</strong>. Seperti halnya situs layanan jual-beli menyediakan sarana jual-beli dari konsumen ke konsumen. Siapa pun dapat membuka toko online di Jevera dan melayani pembeli dari seluruh Indonesia untuk transaksi satuan maupun banyak. er</p>\n	<p style=\"box-sizing: border-box; margin: 0px 0px 15px;\">\n		&nbsp;</p>\n	<div>\n		&nbsp;</div>\n</article>\n', '<div class=\"anchor js-instafilta--section\" helvetica=\"\" id=\"general-terms\" style=\"box-sizing: border-box; margin-bottom: 25px; color: rgb(128, 128, 128); font-family: \">\n	<h6 style=\"box-sizing: border-box; margin: 0px 0px 15px; font-size: 14px; line-height: 1.45em;\">\n		&nbsp;<strong style=\"font-size: 12px;\">Informasi Umum</strong></h6>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jevera sebagai sarana penunjang bisnis berusaha menyediakan berbagai fitur dan layanan untuk menjamin keamanan dan kenyamanan para penggunanya.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jevera tidak berperan sebagai Pelapak barang, melainkan sebagai perantara antara Pelapak dan Pembeli, untuk mengamankan setiap transaksi yang berlangsung di dalam&nbsp;<em>platform</em>&nbsp;Jevera&nbsp;melalui mekanisme BL Payment System. Adanya biaya ekstra (termasuk pajak dan biaya lainnya) atas segala transaksi yang terjadi di Jevera berada di luar kewenangan Jevera sebagai perantara, dan akan diurus oleh pihak-pihak yang bersangkutan (baik Pelapak atau pun Pembeli) sesuai ketentuan yang berlaku di Indonesia.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jevera hanya mengizinkan jual beli barang yang bisa dikirim melalui jasa pengiriman (jasa ekspedisi), sehingga jasa dan kerjasama dagang (<em>franchise</em>) tidak dapat diperdagangkan melalui Jevera terkecuali ada kerja sama resmi dengan pihak Jevera&nbsp;.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Barang-barang yang dapat diperdagangkan di Jevera&nbsp;merupakan barang yang tidak tercantum di daftar &ldquo;Barang Terlarang&rdquo;.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jevera tidak bertanggung jawab atas kualitas barang, proses pengiriman, rusaknya reputasi pihak lain, dan/atau segala bentuk perselisihan yang dapat terjadi antar Pengguna.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jevera memiliki kewenangan untuk mengambil tindakan yang dianggap perlu terhadap akun yang diduga dan/atau terindikasi melakukan penyalahgunaan, memanipulasi, dan/atau melanggar Aturan Penggunaan di Jevera&nbsp;, mulai dari melakukan moderasi, menghentikan layanan &ldquo;Jual Barang&rdquo;, membatasi jumlah pembuatan akun, membatasi atau mengakhiri hak setiap Pengguna untuk menggunakan layanan, maupun menutup akun tersebut tanpa memberikan pemberitahuan atau informasi terlebih dahulu kepada pemilik akun yang bersangkutan.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jevera memiliki kewenangan untuk mengambil tindakan yang dianggap perlu terhadap akun Pengguna, mulai dari melakukan moderasi, menghentikan layanan &ldquo;Jual Barang&rdquo;, membatasi jumlah pembuatan akun, membatasi atau mengakhiri hak setiap Pengguna untuk menggunakan layanan, maupun menutup akun tersebut tanpa memberikan pemberitahuan atau informasi terlebih dahulu kepada pemilik akun yang bersangkutan.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jevera memiliki kewenangan untuk mengambil keputusan atas permasalahan yang terjadi pada setiap transaksi.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jika Pengguna gagal untuk mematuhi setiap ketentuan dalam Aturan Penggunaan di Jevera ini, maka Jevera berhak untuk mengambil tindakan yang dianggap perlu termasuk namun tidak terbatas pada melakukan moderasi, menghentikan layanan &ldquo;Jual Barang&rdquo;, menutup akun dan/atau mengambil langkah hukum selanjutnya.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; BL Payment System bersifat mengikat Pengguna Jevera dan hanya menjamin dana Pembeli tetap aman jika proses transaksi dilakukan dengan Pelapak yang terdaftar di dalam sistem Jevera. Kerugian yang diakibatkan keterlibatan pihak lain di luar Pembeli, Pelapak, dan Jevera, tidak menjadi tanggung jawab Jevera.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jevera berhak meminta data-data pribadi Pengguna jika diperlukan.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Aturan Penggunaan Jevera dapat berubah sewaktu-waktu dan/atau diperbaharui dari waktu ke waktu tanpa pemberitahuan terlebih dahulu. Dengan mengakses Jevera, Pengguna dianggap menyetujui perubahan-perubahan dalam Aturan Penggunaan Jevera.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Aturan Penggunaan Jevera pada Situs Jevera berlaku mutatis mutandis untuk penggunaan Aplikasi Jevera.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Hati-hati terhadap penipuan yang mengatasnamakan Jevera. Untuk informasi dan pengaduan, silakan hubungi cs@Jevera.com.</p>\n	<p>\n		<strong>Pengguna</strong></p>\n	<p style=\"margin-left:21.0pt;\">\n		1.&nbsp;&nbsp;&nbsp;&nbsp; Pengguna wajib mengisi data pribadi secara lengkap dan jujur di halaman akun (profil).</p>\n	<p style=\"margin-left:21.0pt;\">\n		2.&nbsp;&nbsp;&nbsp;&nbsp; Pengguna dilarang mencantumkan alamat, nomor kontak, e-mail, situs, forum, dan media sosial di lapak, termasuk di foto profil, foto header lapak, nama akun (<em>username</em>), nama lapak, dan deskripsi lapak.</p>\n	<p style=\"margin-left:21.0pt;\">\n		3.&nbsp;&nbsp;&nbsp;&nbsp; Pengguna bertanggung jawab atas keamanan dari akun termasuk penggunaan e-mail dan&nbsp;<em>password</em>.</p>\n	<p style=\"margin-left:21.0pt;\">\n		4.&nbsp;&nbsp;&nbsp;&nbsp; Pengguna wajib mengisi data bank pribadi (<em>bank account</em>) untuk kepentingan bertransaksi di Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		5.&nbsp;&nbsp;&nbsp;&nbsp; Penggunaan fasilitas apapun yang disediakan oleh Jevera mengindikasikan bahwa Pengguna telah memahami dan menyetujui segala aturan yang diberlakukan oleh Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		6.&nbsp;&nbsp;&nbsp;&nbsp; Selama berada dalam platform Jevera, Pengguna dilarang keras menyampaikan setiap jenis konten apapun yang menyesatkan, memfitnah, atau mencemarkan nama baik, mengandung atau bersinggungan dengan unsur SARA, diskriminasi, dan/atau menyudutkan pihak lain.</p>\n	<p style=\"margin-left:21.0pt;\">\n		7.&nbsp;&nbsp;&nbsp;&nbsp; Pengguna tidak diperbolehkan menggunakan Jevera untuk melanggar peraturan yang ditetapkan oleh hukum di Indonesia maupun di negara lainnya.</p>\n	<p style=\"margin-left:21.0pt;\">\n		8.&nbsp;&nbsp;&nbsp;&nbsp; Pengguna bertanggung jawab atas segala risiko yang timbul di kemudian hari atas informasi yang diberikannya ke dalam Jevera, termasuk namun tidak terbatas pada hal-hal yang berkaitan dengan hak cipta, merek, desain industri, desain tata letak industri dan hak paten atas suatu produk.</p>\n	<p style=\"margin-left:21.0pt;\">\n		9.&nbsp;&nbsp;&nbsp;&nbsp; Pengguna diwajibkan menghargai hak-hak Pengguna lainnya dengan tidak memberikan informasi pribadi ke pihak lain tanpa izin pihak yang bersangkutan.</p>\n	<p style=\"margin-left:21.0pt;\">\n		10.&nbsp;&nbsp; Pengguna tidak diperkenankan mengirimkan e-mail&nbsp;<em>spam</em>&nbsp;dengan merujuk ke bagian apapun dari Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		11.&nbsp;&nbsp; Administrator Jevera berhak menyesuaikan dan/atau menghapus informasi barang, dan menonaktifkan akun Pengguna.</p>\n	<p style=\"margin-left:21.0pt;\">\n		12.&nbsp;&nbsp; Jevera memiliki hak untuk memblokir penggunaan sistem terhadap Pengguna yang melanggar peraturan perundang-undangan yang berlaku di wilayah Indonesia.</p>\n	<p style=\"margin-left:21.0pt;\">\n		13.&nbsp;&nbsp; Pengguna akan mendapatkan beragam informasi promo terbaru dan penawaran eksklusif. Namun, Pengguna dapat berhenti berlangganan (<em>unsubscribe</em>) jika tidak ingin menerima informasi tersebut.</p>\n	<p style=\"margin-left:21.0pt;\">\n		14.&nbsp;&nbsp; Pengguna dilarang menggunakan logo Jevera di foto profil (avatar).</p>\n	<p style=\"margin-left:21.0pt;\">\n		15.&nbsp;&nbsp; Pengguna dilarang menggunakan kata-kata kasar yang tidak sesuai norma, baik saat berdiskusi di fitur kirim pesan atau chat maupun kolom diskusi retur. Jika ditemukan pelanggaran, Jevera berhak memberikan sanksi seperti menonaktifkan sementara fitur pesan, dan membekukan atau menonaktifkan akun Pengguna.</p>\n	<p style=\"margin-left:21.0pt;\">\n		16.&nbsp;&nbsp; Pengguna dilarang menggunakan fitur kirim pesan atau chat sebagai iklan promosi barang dagangan di Jevera maupun di platform atau situs lain yang dapat mengganggu Pengguna lainnya. Jika ditemukan pelanggaran, Jevera berhak memberikan sanksi seperti menonaktifkan fitur pesan dan/atau akun Pengguna.</p>\n	<p style=\"margin-left:21.0pt;\">\n		17.&nbsp;&nbsp; Pengguna dilarang menggunakan fitur kirim pesan atau chat sebagai sarana penelitian, kuesioner, atau&nbsp;<em>survey</em>. Jika ditemukan pelanggaran, Jevera berhak memberikan sanksi seperti menonaktifkan fitur pesan dan/atau akun Pengguna.</p>\n	<p style=\"margin-left:21.0pt;\">\n		18.&nbsp;&nbsp; Pengguna dilarang melakukan transfer atau menjual akun Pengguna ke Pengguna lain atau ke pihak lain tanpa persetujuan dari Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		19.&nbsp;&nbsp; Pengguna dengan ini menyatakan bahwa Pengguna telah mengetahui seluruh peraturan perundang- undangan yang berlaku di wilayah Republik Indonesia dalam setiap transaksi di Jevera, dan tidak akan melakukan tindakan apapun yang mungkin melanggar peraturan perundang-undangan yang berlaku di wilayah Republik Indonesia.</p>\n	<p style=\"margin-left:21.0pt;\">\n		20.&nbsp;&nbsp; Pengguna dilarang membuat salinan, modifikasi, turunan atau distribusi konten atau mempublikasikan tampilan yang berasal dari Jevera yang dapat melanggar Hak Kekayaan Intelektual Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		21.&nbsp;&nbsp; Pengguna dilarang membuat akun Jevera dengan tujuan menghindari batasan pembelian, penyalahgunaan voucher atau konsekuensi kebijakan Aturan Penggunaan Jevera lainnya.</p>\n	<p>\n		<strong>Jual barang</strong></p>\n	<p style=\"margin-left:21.0pt;\">\n		1.&nbsp;&nbsp;&nbsp;&nbsp; Pelapak bertanggung jawab secara penuh atas segala risiko yang timbul di kemudian hari terkait dengan informasi yang dibuatnya, termasuk, namun tidak terbatas pada hal-hal yang berkaitan dengan hak cipta, merek, desain industri, desain tata letak sirkuit, hak paten dan/atau izin lain yang telah ditetapkan atas suatu produk menurut hukum yang berlaku di Indonesia.</p>\n	<p style=\"margin-left:21.0pt;\">\n		2.&nbsp;&nbsp;&nbsp;&nbsp; Pelapak hanya diperbolehkan menjual barang-barang yang tidak tercantum di daftar &ldquo;Barang Terlarang&rdquo;.</p>\n	<p style=\"margin-left:21.0pt;\">\n		3.&nbsp;&nbsp;&nbsp;&nbsp; Pelapak wajib menempatkan barang dagangan sesuai dengan kategori dan subkategorinya.</p>\n	<p style=\"margin-left:21.0pt;\">\n		4.&nbsp;&nbsp;&nbsp;&nbsp; Pelapak wajib mengisi nama atau judul barang dengan jelas, singkat dan padat.</p>\n	<p style=\"margin-left:21.0pt;\">\n		5.&nbsp;&nbsp;&nbsp;&nbsp; Pelapak wajib menampilkan gambar barang yang sesuai dengan deskripsi barang yang dijual dan tidak mencantumkan logo ataupun alamat situs jual-beli lain pada gambar. Dianjurkan foto atau gambar memperlihatkan 3 bagian (depan, samping dan belakang) dengan resolusi minimal 300px.</p>\n	<p style=\"margin-left:21.0pt;\">\n		6.&nbsp;&nbsp;&nbsp;&nbsp; Pelapak wajib mengisi harga yang sesuai dengan harga sebenarnya.</p>\n	<p style=\"margin-left:21.0pt;\">\n		7.&nbsp;&nbsp;&nbsp;&nbsp; Pelapak tidak diperkenankan mencantumkan alamat (e-mail, situs, forum, dan&nbsp;<em>social network</em>), nomor kontak, ID / PIN /&nbsp;<em>username social media</em>, dan nomor rekening bank selain pada kolom yang disediakan.</p>\n	<p style=\"margin-left:21.0pt;\">\n		8.&nbsp;&nbsp;&nbsp;&nbsp; Pelapak dilarang menjual barang yang identik sama (<em>multiple posting</em>) dengan yang sudah ada di lapaknya.</p>\n	<p style=\"margin-left:21.0pt;\">\n		9.&nbsp;&nbsp;&nbsp;&nbsp; Pelapak dilarang melakukan duplikasi penjualan barang dengan menyalin atau menggunakan gambar dari lapak Pelapak lain.</p>\n	<p style=\"margin-left:21.0pt;\">\n		10.&nbsp;&nbsp; Pelapak tidak perkenankan memberikan informasi alamat (e-mail, situs, forum, dan&nbsp;<em>social network</em>), nomor kontak, ID / PIN /&nbsp;<em>username social media</em>&nbsp;melalui fitur pesan.</p>\n	<p style=\"margin-left:21.0pt;\">\n		11.&nbsp;&nbsp; Pelapak wajib memperbarui (<em>update</em>) ketersediaan dan status barang yang dijual.</p>\n	<p style=\"margin-left:21.0pt;\">\n		12.&nbsp;&nbsp; Catatan Pelapak diperuntukkan bagi Pelapak yang ingin memberikan catatan tambahan yang tidak terkait dengan deskripsi barang kepada calon Pembeli. Catatan Pelapak tetap tunduk terhadap Aturan Penggunaan Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		13.&nbsp;&nbsp; Pelapak wajib mengisi kolom Deskripsi Barang sesuai dengan Aturan Penggunaan di Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		14.&nbsp;&nbsp; Pelapak dilarang membuat transaksi fiktif atau palsu demi kepentingan menaikkan&nbsp;<em>feedback</em>. Jevera berhak mengambil tindakan seperti pemblokiran akun atau tindakan lainnya apabila ditemukan tindakan kecurangan.</p>\n	<p style=\"margin-left:21.0pt;\">\n		15.&nbsp;&nbsp; Pelapak wajib mengirimkan barang menggunakan jasa ekspedisi sesuai dengan yang dipilih oleh Pembeli pada saat melakukan transaksi di dalam sistem Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		16.&nbsp;&nbsp; Apabila Pelapak menggunakan jasa ekspedisi yang berbeda dengan jasa dan/atau jenis jasa ekspedisi yang dipilih oleh Pembeli pada saat melakukan transaksi di dalam sistem Jevera maka Pelapak bertanggung jawab atas segala hal selama proses pengiriman yang disebabkan oleh penggunaan jasa dan/atau jenis jasa ekspedisi yang berbeda tersebut.</p>\n	<p style=\"margin-left:21.0pt;\">\n		17.&nbsp;&nbsp; Pelapak memahami dan menyetujui bahwa kekurangan dana biaya kirim yang disebabkan oleh penggunaan jasa dan/atau jenis jasa yang berbeda dari pilihan Pembeli pada saat melakukan transaksi di dalam sistem Jevera merupakan tanggung jawab Pelapak terkecuali perbedaan tersebut atas permintaan Pembeli.</p>\n	<p style=\"margin-left:21.0pt;\">\n		18.&nbsp;&nbsp; Pembeli berhak atas kelebihan dana dari biaya kirim yang diakibatkan perbedaan penggunaan jasa dan/atau jenis jasa ekspedisi oleh Pelapak dari pilihan Pembeli pada saat melakukan transaksi di dalam sistem Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		19.&nbsp;&nbsp; Pelapak wajib memenuhi ketentuan yang sudah ditetapkan oleh pihak jasa ekspedisi berkaitan dengan&nbsp;<em>packing</em>&nbsp;barang yang aman serta menggunakan asuransi dan/atau&nbsp;<em>packing</em>&nbsp;kayu pada barang-barang tertentu sehingga apabila barang rusak atau hilang Pelapak dapat mengajukan klaim ke pihak jasa ekspedisi.</p>\n	<p>\n		<strong>Transaksi</strong></p>\n	<p style=\"margin-left:21.0pt;\">\n		1.&nbsp;&nbsp;&nbsp;&nbsp; Demi keamanan dan kenyamanan para Pengguna, setiap transaksi jual-beli di Jevera diwajibkan untuk menggunakan BL Payment System. Untuk informasi mengenai penggunaan BL Payment System dapat dipelajari di&nbsp;<a href=\"https://panduan.bukalapak.com/\">Panduan Jevera</a></p>\n	<p style=\"margin-left:21.0pt;\">\n		2.&nbsp;&nbsp;&nbsp;&nbsp; Pembeli wajib transfer sesuai dengan nominal total belanja dari transaksi dalam waktu 1x10 jam (dengan asumsi Pembeli telah mempelajari informasi barang yang telah dipesannya). Jika dalam waktu 1x10 jam barang dipesan tetapi Pembeli tidak mentransfer dana maka transaksi akan dibatalkan secara otomatis.</p>\n	<p style=\"margin-left:21.0pt;\">\n		3.&nbsp;&nbsp;&nbsp;&nbsp; Setiap transaksi di Jevera yang menggunakan metode transfer akan dikenakan biaya operasional dalam bentuk kode unik pembayaran yang ditanggung oleh Pembeli.</p>\n	<p style=\"margin-left:21.0pt;\">\n		4.&nbsp;&nbsp;&nbsp;&nbsp; Pembeli tidak dapat membatalkan transaksi setelah melunasi pembayaran.</p>\n	<p style=\"margin-left:21.0pt;\">\n		5.&nbsp;&nbsp;&nbsp;&nbsp; Pelapak wajib mengirimkan barang dan mendaftarkan nomor resi pengiriman yang benar dan asli setelah status transaksi &ldquo;Dibayar&rdquo;. Satu nomor resi hanya berlaku untuk satu nomor transaksi di Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		6.&nbsp;&nbsp;&nbsp;&nbsp; Jika Pelapak tidak mengirimkan barang dalam batas waktu pengiriman sejak pembayaran (2x24 jam kerja untuk biaya pengiriman reguler atau 2x24 jam untuk biaya pengiriman kilat), maka Pelapak dianggap telah menolak pesanan. Sehingga, sistem secara otomatis memberikan&nbsp;<em>feedback</em>&nbsp;negatif dan reputasi tolak pesanan, serta mengembalikan seluruh dana (<em>refund</em>) ke Pembeli.</p>\n	<p style=\"margin-left:21.0pt;\">\n		7.&nbsp;&nbsp;&nbsp;&nbsp; Pengembalian dana transaksi dilakukan dengan menambahkan saldo BukaDompet ke Pembeli. Untuk pembayaran dengan menggunakan kartu kredit, dana transaksi akan dikembalikan langsung ke kartu kredit. Pengembalian dana dilakukan dengan memberikan pengurangan biaya pada kartu kredit Pembeli dalam waktu&nbsp;<strong>maksimal 14 hari kerja</strong>&nbsp;setelah pembayaran.</p>\n	<p style=\"margin-left:21.0pt;\">\n		8.&nbsp;&nbsp;&nbsp;&nbsp; Fitur Item Replacement Jevera akan otomatis mencarikan barang yang sama jika transaksi ditolak oleh Pelapak. Jika barang yang sama tidak ditemukan, maka dana akan dikembalikan ke BukaDompet pembeli.</p>\n	<p style=\"margin-left:21.0pt;\">\n		9.&nbsp;&nbsp;&nbsp;&nbsp; Jevera akan mengirimkan email konfirmasi pencarian barang pengganti melalui fitur Item Replacement jika transaksi diabaikan oleh Pelapak. Apabila Pembeli tidak melakukan konfirmasi dalam waktu 1x6 jam, maka fitur Item Replacement Jevera akan otomatis mencarikan barang pengganti.</p>\n	<p style=\"margin-left:21.0pt;\">\n		10.&nbsp;&nbsp; Jika Pembeli melakukan konfirmasi bahwa tidak berkenan untuk dicarikan barang pengganti melalui email yang dikirim Jevera, maka fitur Item Replacement Jevera akan otomatis mengembalikan dana ke BukaDompet.</p>\n	<p style=\"margin-left:21.0pt;\">\n		11.&nbsp;&nbsp; Untuk Pembeli yang memiliki akun di Jevera, apabila terdapat selisih harga barang pengganti, maka dana selisih akan ditanggung oleh Jevera jika harga barang pengganti lebih mahal dan selisih harga akan dikembalikan ke BukaDompet jika harga barang pengganti lebih murah setelah transaksi selesai.</p>\n	<p style=\"margin-left:21.0pt;\">\n		12.&nbsp;&nbsp; Untuk Pembeli yang tidak memiliki akun di Jevera, apabila terdapat selisih harga barang pengganti, maka dana selisih akan ditanggung oleh Jevera jika harga barang pengganti lebih mahal dan jika harga barang pengganti lebih murah maka selisih harga akan hangus.</p>\n	<p style=\"margin-left:21.0pt;\">\n		13.&nbsp;&nbsp; Transaksi yang menggunakan metode pembayaran kartu kredit dan Kredivo tidak akan diproses oleh fitur Item Replacement apabila transaksi ditolak atau diabaikan oleh Pelapak.</p>\n	<p style=\"margin-left:21.0pt;\">\n		14.&nbsp;&nbsp; Sistem Jevera secara otomatis mengecek status pengiriman barang melalui nomor resi yang diberikan Pelapak. Seluruh dana akan dikembalikan ke Pembeli apabila nomor resi terdeteksi tidak valid dan Pelapak tidak melakukan ubah resi valid dalam 1x24 jam. Jika Pelapak memasukkan nomor resi tidak valid lebih dari satu kali maka Jevera akan mengembalikan seluruh dana transaksi kepada Pembeli dan Pelapak mendapatkan&nbsp;<em>feedback</em>&nbsp;negatif.</p>\n	<p style=\"margin-left:21.0pt;\">\n		15.&nbsp;&nbsp; Jika Pembeli tidak memberikan konfirmasi penerimaan barang dalam waktu 2x24 jam sejak barang diterima yang dinyatakan di sistem&nbsp;<em>tracking</em>&nbsp;jasa pengiriman, Jevera akan mentransfer dana langsung ke BukaDompet Pelapak tanpa memberikan konfirmasi ke Pembeli.</p>\n	<p style=\"margin-left:21.0pt;\">\n		16.&nbsp;&nbsp; Sistem secara otomatis memberikan&nbsp;<em>feedback</em>&nbsp;(rekomendasi) positif dan mentransfer dana pembayaran ke BukaDompet Pelapak jika status resi menunjukkan &lsquo;Barang diterima&rsquo; dan Pembeli telah melewati batas waktu untuk konfirmasi.</p>\n	<p style=\"margin-left:21.0pt;\">\n		17.&nbsp;&nbsp; Pembeli dapat memperbarui&nbsp;<em>feedback</em>&nbsp;maksimal 3x24 jam setelah transaksi dinyatakan selesai oleh sistem Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		18.&nbsp;&nbsp; Retur (pengembalian barang) hanya diperbolehkan jika kesalahan dilakukan oleh Pelapak dan barang tidak sesuai deskripsi.</p>\n	<p style=\"margin-left:21.0pt;\">\n		19.&nbsp;&nbsp; Retur tidak dapat dilakukan setelah transaksi selesai menurut sistem general tracking Jevera atau Pembeli telah melakukan konfirmasi barang diterima dan tidak memilih retur.</p>\n	<p style=\"margin-left:21.0pt;\">\n		20.&nbsp;&nbsp; Langkah-langkah melakukan retur bisa dibaca pada&nbsp;<a href=\"http://www.bukalapak.com/faqs/24\">halaman ini</a></p>\n	<p style=\"margin-left:21.0pt;\">\n		21.&nbsp;&nbsp; Jevera akan menahan dana hingga ada kesepakatan (antara Pembeli dan Pelapak) apakah akan dilakukan pengembalian barang ke Pelapak atau tidak.</p>\n	<p style=\"margin-left:21.0pt;\">\n		22.&nbsp;&nbsp; Jevera akan mengembalikan dana transaksi ke Pembeli jika dalam waktu 5x24 jam Pelapak tidak merespon pesan permintaan retur dari Pembeli di halaman detail transaksi. Selanjutnya, Pembeli wajib mengirimkan barang tersebut ke kantor Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		23.&nbsp;&nbsp; Jevera tidak bertanggung jawab terhadap barang retur di kantor Jevera apabila Pelapak tidak melakukan pengaduan kepemilikan barang dalam waktu 30 hari sejak barang diterima di kantor Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		24.&nbsp;&nbsp; Pembeli wajib mengirimkan barang ke Pelapak dan menginformasikan nomor resi ke Jevera jika ada kesepakatan retur dengan Pelapak.</p>\n	<p style=\"margin-left:21.0pt;\">\n		25.&nbsp;&nbsp; Jevera hanya memantau retur sampai barang diterima kembali oleh Pelapak.</p>\n	<p style=\"margin-left:21.0pt;\">\n		26.&nbsp;&nbsp; Jevera berhak melakukan&nbsp;<em>refund</em>&nbsp;dana ke Pembeli jika barang retur telah sampai di kantor Jevera dan berdasarkan pengecekan sesuai dengan yang dikeluhkan Pembeli.</p>\n	<p style=\"margin-left:21.0pt;\">\n		27.&nbsp;&nbsp; Jevera atas kebijakannya sendiri dapat melakukan penahanan atau pembekuan BukaDompet untuk melakukan perlindungan terhadap segala risiko dan kerugian yang timbul, jika Jevera menyimpulkan bahwa tindakan Pengguna, baik Pelapak maupun Pembeli terindikasi melakukan kecurangan-kecurangan atau penyalahgunaan dalam bertransaksi dan/atau pelanggaran terhadap Aturan Penggunaan Jevera dan jika akun Pengguna diduga atau terindikasi telah diakses oleh pihak lain.</p>\n	<p>\n		<strong>Penggunaan Voucher</strong></p>\n	<p style=\"margin-left:21.0pt;\">\n		1.&nbsp;&nbsp;&nbsp;&nbsp; Voucher hanya berlaku untuk transaksi dengan pengiriman yang menggunakan jasa ekspedisi yang tersedia di Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		2.&nbsp;&nbsp;&nbsp;&nbsp; Jevera berhak melakukan tindakan-tindakan yang diperlukan tanpa pemberitahuan sebelumnya. Tindakan tersebut seperti pembatalan transaksi, pembatalan voucher, pemblokiran akun Pengguna, atau tindakan lainnya apabila ditemukan kecurangan dari Pengguna.</p>\n	<p style=\"margin-left:21.0pt;\">\n		3.&nbsp;&nbsp;&nbsp;&nbsp; Jevera berhak melakukan pembatalan transaksi atau membatalkan penggunaan voucher sewaktu-waktu tanpa pemberitahuan terlebih dahulu kepada Pengguna.</p>\n	<p style=\"margin-left:21.0pt;\">\n		4.&nbsp;&nbsp;&nbsp;&nbsp; Jevera berhak mengubah syarat dan ketentuan sewaktu-waktu tanpa pemberitahuan terlebih dahulu kepada Pengguna.</p>\n	<p>\n		<strong>Barang Terlarang</strong></p>\n	<p>\n		Jevera telah dan akan terus melakukan hal-hal sebagaimana dipersyaratkan oleh peraturan perundang-undangan untuk mencegah terjadinya perdagangan barang-barang yang melanggar ketentuan hukum yang berlaku dan/atau hak pribadi pihak ketiga. Berkenaan dengan hal tersebut, berikut adalah barang-barang yang dilarang untuk diperjualbelikan melalui Jevera:</p>\n	<p style=\"margin-left:21.0pt;\">\n		1.&nbsp;&nbsp;&nbsp;&nbsp; Segala bentuk tulisan yang dapat berpengaruh negatif terhadap Jevera</p>\n	<p style=\"margin-left:21.0pt;\">\n		2.&nbsp;&nbsp;&nbsp;&nbsp; Narkotika, obat-obat tidak terdaftar di Dinkes dan/atau BPOM</p>\n	<p style=\"margin-left:21.0pt;\">\n		3.&nbsp;&nbsp;&nbsp;&nbsp; Senjata api, kelengkapan senjata api, replika senjata api, airsoft gun, air gun, dan peluru atau sejenis peluru, senjata tajam, serta jenis senjata lainnya</p>\n	<p style=\"margin-left:21.0pt;\">\n		4.&nbsp;&nbsp;&nbsp;&nbsp; Dokumen pemerintahan dan perjalanan</p>\n	<p style=\"margin-left:21.0pt;\">\n		5.&nbsp;&nbsp;&nbsp;&nbsp; Bagian/organ manusia</p>\n	<p style=\"margin-left:21.0pt;\">\n		6.&nbsp;&nbsp;&nbsp;&nbsp; Mailing list dan informasi pribadi</p>\n	<p style=\"margin-left:21.0pt;\">\n		7.&nbsp;&nbsp;&nbsp;&nbsp; Barang-barang yang melecehkan pihak/ras tertentu atau dapat menyinggung perasaan orang lain</p>\n	<p style=\"margin-left:21.0pt;\">\n		8.&nbsp;&nbsp;&nbsp;&nbsp; Barang yang berhubungan dengan kepolisian</p>\n	<p style=\"margin-left:21.0pt;\">\n		9.&nbsp;&nbsp;&nbsp;&nbsp; Barang yang belum tersedia (<em>pre order</em>) terkecuali sanggup kirim barang dalam waktu 2x24 jam kerja sejak transaksi terbayar</p>\n	<p style=\"margin-left:21.0pt;\">\n		10.&nbsp;&nbsp; Barang curian</p>\n	<p style=\"margin-left:21.0pt;\">\n		11.&nbsp;&nbsp; Barang mistis</p>\n	<p style=\"margin-left:21.0pt;\">\n		12.&nbsp;&nbsp; Pembuka kunci dan segala aksesori penunjang tindakan perampokan/pencurian</p>\n	<p style=\"margin-left:21.0pt;\">\n		13.&nbsp;&nbsp; Barang yang dapat dan atau mudah meledak, menyala atau terbakar sendiri</p>\n	<p style=\"margin-left:21.0pt;\">\n		14.&nbsp;&nbsp; Pornografi,&nbsp;<em>sex toys</em>, alat untuk memperbesar organ vital pria, maupun barang asusila lainnya</p>\n	<p style=\"margin-left:21.0pt;\">\n		15.&nbsp;&nbsp; Barang cetakan/rekaman yang isinya dapat mengganggu keamanan &amp; ketertiban serta stabilitas nasional</p>\n	<p style=\"margin-left:21.0pt;\">\n		16.&nbsp;&nbsp; <em>E-Book</em>, CD, DVD, dan&nbsp;<em>Software</em>&nbsp;bajakan</p>\n	<p style=\"margin-left:21.0pt;\">\n		17.&nbsp;&nbsp; Segala jenis binatang atau hewan peliharaan</p>\n	<p style=\"margin-left:21.0pt;\">\n		18.&nbsp;&nbsp; Jasa, donasi, sewa menyewa, promo&nbsp;<em>event</em>&nbsp;dan sejenisnya terkecuali ada kerja sama resmi dengan pihak Jevera</p>\n	<p style=\"margin-left:21.0pt;\">\n		19.&nbsp;&nbsp; Merek dagang</p>\n	<p style=\"margin-left:21.0pt;\">\n		20.&nbsp;&nbsp; Otomotif (Mobil dan Motor) terkecuali ada kerja sama resmi dengan pihak Jevera</p>\n	<p style=\"margin-left:21.0pt;\">\n		21.&nbsp;&nbsp; Velg Mobil</p>\n	<p style=\"margin-left:21.0pt;\">\n		22.&nbsp;&nbsp; Properti (Rumah, Tanah, dan lain-lain)</p>\n	<p style=\"margin-left:21.0pt;\">\n		23.&nbsp;&nbsp; Pulsa elektrik maupun pulsa fisik/voucher, voucher kuota internet, voucher game, voucher aplikasi, steam wallet, dan lainnya; terkecuali ada kerja sama resmi dengan pihak Jevera</p>\n	<p style=\"margin-left:21.0pt;\">\n		24.&nbsp;&nbsp; Produk yang bukan produk asli dengan merek, atau berkaitan dengan merek terdaftar.</p>\n	<p style=\"margin-left:21.0pt;\">\n		25.&nbsp;&nbsp; Gadget (ponsel, tablet, phablet,&nbsp;<em>smartwatch</em>, dan sejenisnya) replika atau berasal dari pasar gelap (<em>black market</em>)</p>\n	<p style=\"margin-left:21.0pt;\">\n		26.&nbsp;&nbsp; Barang-barang lain yang dilarang untuk diperjualbelikan secara bebas berdasarkan hukum yang berlaku di Indonesia</p>\n	<p>\n		<strong>Sanksi</strong></p>\n	<p>\n		Segala tindakan yang melanggar peraturan di Jevera akan dikenakan sanksi berupa termasuk namun tidak terbatas pada:</p>\n	<p style=\"margin-left:21.0pt;\">\n		1.&nbsp;&nbsp;&nbsp;&nbsp; Pelapak mendapatkan 1&nbsp;<em>feedback</em>&nbsp;negatif apabila tidak mengirimkan barang dalam batas waktu pengiriman sejak pembayaran (2x24 jam kerja untuk biaya pengiriman reguler atau 2x24 jam untuk biaya pengiriman kilat).</p>\n	<p style=\"margin-left:21.0pt;\">\n		2.&nbsp;&nbsp;&nbsp;&nbsp; Pelapak mendapatkan 1&nbsp;<em>feedback</em>&nbsp;negatif jika sudah 5 kali menolak pesanan.</p>\n	<p style=\"margin-left:21.0pt;\">\n		3.&nbsp;&nbsp;&nbsp;&nbsp; Pelapak mendapatkan 3&nbsp;<em>feedback</em>&nbsp;negatif jika sudah memroses pesanan namun tidak kirim barang dalam batas waktu pengiriman sejak pembayaran (2x24 jam kerja untuk pengiriman reguler atau 2x24 jam untuk pengiriman kilat).</p>\n	<p style=\"margin-left:21.0pt;\">\n		4.&nbsp;&nbsp;&nbsp;&nbsp; Akun dibekukan.</p>\n	<p style=\"margin-left:21.0pt;\">\n		5.&nbsp;&nbsp;&nbsp;&nbsp; Akun dinonaktifkan. Dan jika ada Paket Push di akun maka Paket Push hangus.</p>\n	<p style=\"margin-left:21.0pt;\">\n		6.&nbsp;&nbsp;&nbsp;&nbsp; Pelaporan ke pihak terkait (Kepolisian, dll).</p>\n	<p>\n		<strong>Pembatasan Tanggung Jawab</strong></p>\n	<p style=\"margin-left:21.0pt;\">\n		1.&nbsp;&nbsp;&nbsp;&nbsp; Jevera tidak bertanggung jawab atas segala risiko dan kerugian yang timbul dari dan dalam kaitannya dengan informasi yang dituliskan oleh pengguna Jevera.</p>\n	<p style=\"margin-left:21.0pt;\">\n		2.&nbsp;&nbsp;&nbsp;&nbsp; Jevera tidak bertanggung jawab atas segala pelanggaran hak cipta, merek, desain industri, desain tata letak sirkuit, hak paten atau hak-hak pribadi lain yang melekat atas suatu barang, berkenaan dengan segala informasi yang dibuat oleh Pelapak. Untuk melaporkan pelanggaran hak cipta, merek, desain industri, desain tata letak sirkuit, hak paten atau hak-hak pribadi lain,&nbsp;<a href=\"https://www.bukalapak.com/supports/contact_us\">klik di sini</a></p>\n	<p style=\"margin-left:21.0pt;\">\n		3.&nbsp;&nbsp;&nbsp;&nbsp; Jevera tidak bertanggung jawab atas segala risiko dan kerugian yang timbul berkenaan dengan penggunaan barang yang dibeli melalui Jevera, dalam hal terjadi pelanggaran peraturan perundang-undangan.</p>\n	<p style=\"margin-left:21.0pt;\">\n		4.&nbsp;&nbsp;&nbsp;&nbsp; Jevera tidak bertanggung jawab atas segala risiko dan kerugian yang timbul berkenaan dengan diaksesnya akun Pengguna oleh pihak lain.</p>\n	<p style=\"margin-left:21.0pt;\">\n		5.&nbsp;&nbsp;&nbsp;&nbsp; Jevera tidak bertanggung jawab atas segala risiko dan kerugian yang timbul akibat transaksi di luar BL Payment System.</p>\n	<p style=\"margin-left:21.0pt;\">\n		6.&nbsp;&nbsp;&nbsp;&nbsp; Jevera tidak bertanggung jawab atas segala risiko dan kerugian yang timbul akibat kesalahan atau perbedaan nominal yang seharusnya ditransfer ke rekening atas nama PT.Jevera.com.</p>\n	<p style=\"margin-left:21.0pt;\">\n		7.&nbsp;&nbsp;&nbsp;&nbsp; Jevera tidak bertanggung jawab atas segala risiko dan kerugian yang timbul apabila transaksi telah selesai secara sistem (dana telah masuk ke BukaDompet Pelapak ataupun Pembeli).</p>\n	<p style=\"margin-left:21.0pt;\">\n		8.&nbsp;&nbsp;&nbsp;&nbsp; Jevera tidak bertanggung jawab atas segala risiko dan kerugian yang timbul akibat kehilangan barang ketika proses transaksi berjalan dan/atau selesai.</p>\n	<p style=\"margin-left:21.0pt;\">\n		9.&nbsp;&nbsp;&nbsp;&nbsp; Jevera tidak bertanggung jawab atas segala risiko dan kerugian yang timbul akibat kesalahan Pengguna ataupun pihak lain dalam transfer dana ke rekening PT.Jevera.com.</p>\n	<p style=\"margin-left:21.0pt;\">\n		10.&nbsp;&nbsp; Jevera tidak bertanggung jawab atas segala risiko dan kerugian yang timbul apabila akun dalam keadaan dibekukan dan/atau dinonaktifkan.</p>\n	<p style=\"margin-left:21.0pt;\">\n		11.&nbsp;&nbsp; Dalam keadaan apapun, Pengguna akan membayar kerugian Jevera dan/atau menghindarkan Jevera (termasuk petugas, direktur, karyawan, agen, dan lainnya) dari setiap biaya kerugian apapun, kehilangan, pengeluaran atau kerusakan yang berasal dari tuntutan atau klaim Pihak ke-tiga yang timbul dari pelanggaran Pengguna terhadap Aturan Penggunaan Jevera, dan/atau pelanggaran terhadap hak dari pihak ke-tiga.</p>\n	<p>\n		<strong>Hukum yang Berlaku dan Penyelesaian Sengketa</strong></p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Aturan Penggunaan ini dilaksanakan dan tunduk pada Peraturan Perundang- udangan Republik Indonesia.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Apabila terjadi perselisihan, sebelum beralih ke alternatif lain, Pengguna wajib terlebih dahulu menghubungi Jevera secara langsung agar dapat melakukan perundingan atau musyawarah untuk mencapai resolusi bagi kedua belah pihak.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sebelum menghubungi Jevera secara langsung untuk melakukan perundingan penyelesaian masalah atau sengketa, Pengguna setuju untuk tidak mengumumkan, membuat tulisan-tulisan di media online maupun cetak terkait permasalahan aquo yang dapat menyudutkan Jevera (termasuk petugas, direktur, karyawan dan agen).</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Apabila dalam waktu 1 (satu) bulan setelah dimulainya perundingan atau musyawarah tidak mencapai resolusi, maka PARA PIHAK akan menyelesaikan perselisihan tersebut melalui Pengadilan Negeri Jakarta Selatan.</p>\n	<p>\n		&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Selama perselisihan dalam proses penyelesaian, Pengguna wajib untuk tetap melaksanakan kewajiban-kewajiban lainnya menurut Aturan Penggunaan Jevera.</p>\n	<p>\n		&nbsp;</p>\n</div>\n<p>\n	&nbsp;</p>\n', 'Jln Blimbing no 34 Malang Jawa Timur', '08994278282', 'mail@jevera.coma', '<p>\n	panduan</p>\n');

-- ----------------------------
-- Table structure for log_aktivitas
-- ----------------------------
DROP TABLE IF EXISTS `log_aktivitas`;
CREATE TABLE `log_aktivitas` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(255) NOT NULL,
  `aktivitas` text NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of log_aktivitas
-- ----------------------------

-- ----------------------------
-- Table structure for mainmenu
-- ----------------------------
DROP TABLE IF EXISTS `mainmenu`;
CREATE TABLE `mainmenu` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `idmenu` int(11) NOT NULL,
  `nama_menu` varchar(50) NOT NULL,
  `active_menu` varchar(50) NOT NULL,
  `icon_class` varchar(50) NOT NULL,
  `link_menu` varchar(50) NOT NULL,
  `menu_akses` varchar(12) NOT NULL,
  `entry_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mainmenu
-- ----------------------------
INSERT INTO `mainmenu` VALUES ('1', '1', 'Dashboard', '', 'menu-icon fa fa-tachometer', 'Home', '', '2017-10-17 12:28:54', null);
INSERT INTO `mainmenu` VALUES ('2', '2', 'Produk', '', 'menu-icon fa fa-newspaper-o', 'Produk', '', '2018-09-24 10:17:43', null);
INSERT INTO `mainmenu` VALUES ('19', '3', 'Order', '', 'menu-icon fa fa-newspaper-o', 'Order', '', '2018-09-24 13:03:55', null);
INSERT INTO `mainmenu` VALUES ('20', '4', 'About', '', 'menu-icon fa fa-newspaper-o', 'About', '', '2018-09-24 15:26:04', null);
INSERT INTO `mainmenu` VALUES ('21', '5', 'Kontak', '', 'menu-icon fa fa-newspaper-o', 'Kontak', '', '2018-09-24 15:37:07', null);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(50) NOT NULL,
  `foto_menu` varchar(100) NOT NULL,
  `title_menu` varchar(50) NOT NULL,
  `meta_description_menu` varchar(100) NOT NULL,
  `meta_keywords_menu` varchar(100) NOT NULL,
  `aktif_menu` int(11) NOT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Properti (Rumah, dll)', 'Jual_Beli_Property_foto.jpg', 'Jual Beli Properti Rumah Mobil Ruko Tanah Kontraka', 'Jual Beli Properti Rumah Mobil Ruko Tanah Kontrakan Murah di Malang  - Wahmurah.com', 'Rumah Murah Malang, Mobil Murah Malang, Properti Murah Malang', '0');
INSERT INTO `menu` VALUES ('2', 'Menu Baru', 'Menu_Baru_foto.jpg', 'Baru', 'Baru', 'Baru', '0');
INSERT INTO `menu` VALUES ('4', 'Foods', 'Makanan_foto.jpg', 'Foods', 'Foods', 'Foods', '1');
INSERT INTO `menu` VALUES ('5', 'Seasoning', 'Seasoning.jpg', 'Seasoning', 'Seasoning', 'Seasoning', '1');
INSERT INTO `menu` VALUES ('6', 'Electronics', 'Electronics_foto.jpg', 'Electronics', 'Electronics', 'Electronics', '0');

-- ----------------------------
-- Table structure for ongkir_pembeli
-- ----------------------------
DROP TABLE IF EXISTS `ongkir_pembeli`;
CREATE TABLE `ongkir_pembeli` (
  `id_ongkir` int(11) NOT NULL AUTO_INCREMENT,
  `ongkir` double NOT NULL,
  `id_order` varchar(50) NOT NULL,
  `id_penjual` varchar(50) NOT NULL,
  `tagihan_admin` int(11) NOT NULL DEFAULT '0',
  `pembayaran` int(11) NOT NULL DEFAULT '0',
  `jasa_pengiriman` varchar(255) NOT NULL,
  PRIMARY KEY (`id_ongkir`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ongkir_pembeli
-- ----------------------------
INSERT INTO `ongkir_pembeli` VALUES ('1', '19000', 'T170131001', 'eka', '19000', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('2', '22000', 'T170201001', 'eka', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('3', '32000', 'T170201002', 'eka', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('4', '19000', 'T170202001', 'eka', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('5', '34000', 'T170202002', 'eka', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('6', '5000', 'T170417001', 'wildan', '0', '0', 'JNE - CTC');
INSERT INTO `ongkir_pembeli` VALUES ('7', '7000', 'T170517001', 'wildanafif', '7000', '0', 'POS - Surat Kilat Khusus');
INSERT INTO `ongkir_pembeli` VALUES ('8', '4000', 'T170520001', 'afif', '0', '4000', 'JNE - CTCOKE');
INSERT INTO `ongkir_pembeli` VALUES ('9', '40000', 'T180920001', 'yayanraw', '0', '0', 'JNE - OKE');
INSERT INTO `ongkir_pembeli` VALUES ('10', '5000', 'T180922001', 'SMKN4Malang', '0', '0', 'JNE - OKE');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id_order` varchar(11) NOT NULL,
  `tgl_order` date NOT NULL,
  `total_order` int(11) NOT NULL,
  `status_order` varchar(30) NOT NULL DEFAULT 'Transaksi Belum Bayar' COMMENT 'transaksi belum bayar,menunggu konfirmasi, sudah bayar',
  `nama_order` varchar(30) NOT NULL,
  `email_order` varchar(50) DEFAULT NULL,
  `alamat_order` text NOT NULL,
  `tlp_order` varchar(20) NOT NULL,
  `kode_pos_order` varchar(6) NOT NULL,
  `provinsi_order` varchar(40) NOT NULL,
  `kota_order` varchar(50) DEFAULT NULL,
  `ongkir_order` int(11) NOT NULL,
  `grand_total_order` int(11) NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('T180920001', '2018-09-20', '40000', 'Menunggu Konfirmasi', 'ali', 'imokhammadali4@gmail.com', 'kjasd', '082822728', '719289', 'Bangka Belitung', 'Bangka Barat', '40000', '80000');
INSERT INTO `order` VALUES ('T180922001', '2018-09-22', '80000', 'Transaksi Belum Bayar', 'edwin yordan', 'edwinlaksono12@gmail.com', 'Address*', '85607369653', '64181', '', 'Malang', '5000', '85000');
INSERT INTO `order` VALUES ('T180922002', '2018-09-22', '22000', 'Transaksi Belum Bayar', 'edwin yordan', 'edwinlaksono12@gmail.com', 'Address*', '85607369653', '64181', '', 'Malang', '0', '0');
INSERT INTO `order` VALUES ('T180922003', '2018-09-22', '22000', 'Transaksi Belum Bayar', 'edwin yordan', 'edwinlaksono12@gmail.com', 'Address*', '85607369653', '64181', '', 'Malang', '0', '22000');

-- ----------------------------
-- Table structure for pemasukan
-- ----------------------------
DROP TABLE IF EXISTS `pemasukan`;
CREATE TABLE `pemasukan` (
  `id_pemasukan` int(11) NOT NULL AUTO_INCREMENT,
  `id_detail_order` int(11) NOT NULL,
  `jumlah_pemasukan` int(11) NOT NULL,
  PRIMARY KEY (`id_pemasukan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pemasukan
-- ----------------------------
INSERT INTO `pemasukan` VALUES ('1', '2', '80000');
INSERT INTO `pemasukan` VALUES ('2', '3', '90000');

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(50) NOT NULL,
  `kode_produk` varchar(20) NOT NULL,
  `nama_produk` varchar(255) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `kategori_produk` int(11) NOT NULL,
  `harga` double NOT NULL,
  `berat` int(11) NOT NULL,
  `stok_produk` varchar(20) NOT NULL,
  `ket` text NOT NULL,
  `deskripsi` text NOT NULL,
  `foto_produk1` varchar(50) DEFAULT NULL,
  `foto_produk2` varchar(50) NOT NULL,
  `foto_produk3` varchar(50) NOT NULL,
  `jumlah_stok` int(11) NOT NULL,
  `jumlah_terjual` int(11) NOT NULL,
  `provinsi` int(11) NOT NULL DEFAULT '11',
  `kota` int(11) NOT NULL DEFAULT '255',
  `validasi` tinyint(1) NOT NULL DEFAULT '0',
  `pajak` int(12) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id_produk`)
) ENGINE=InnoDB AUTO_INCREMENT=1809240002 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk
-- ----------------------------
INSERT INTO `produk` VALUES ('121', 'admin', '111', 'IKLAN LAYANAN MASYARAKAT', '0', '1', '22000', '100', 'Ada', '', 'IKLAN LAYANAN MASYARAKAT', null, '', '', '10001', '0', '0', '0', '1', '10');
INSERT INTO `produk` VALUES ('122', 'SMKN4Malang', '', 'COMPANY PROFILE', '0', '1', '35000', '100', 'Ada', '', 'COMPANY PROFILE', '2.jpg', '', '', '1000', '0', '0', '0', '1', '10');
INSERT INTO `produk` VALUES ('123', 'SMKN4Malang', '', 'VIDEO PAKET WISUDA', '0', '1', '40000', '100', 'Ada', '', 'VIDEO PAKET WISUDA', '3.jpg', '', '', '1000', '0', '0', '0', '1', '10');
INSERT INTO `produk` VALUES ('1809240001', '', '122', 'Teh', '0', '1', '1', '1', 'Ada', '', ' ', null, '', '', '1', '0', '11', '255', '0', '10');

-- ----------------------------
-- Table structure for status_order
-- ----------------------------
DROP TABLE IF EXISTS `status_order`;
CREATE TABLE `status_order` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `desk_status` varchar(255) NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of status_order
-- ----------------------------
INSERT INTO `status_order` VALUES ('1', 'Belum Dibayar, Belum Dikonfirmasi, Belum Dikirim');
INSERT INTO `status_order` VALUES ('2', 'Sudah Dibayar, Belum Dikonfirmasi, Belum Dikirim');
INSERT INTO `status_order` VALUES ('3', 'Sudah Dibayar, Sudah Dikonfirmasi, Belum Dikirim');
INSERT INTO `status_order` VALUES ('4', 'Sudah Dibayar, Sudah Dikonfirmasi, Sudah Dikirim');

-- ----------------------------
-- Table structure for submenu
-- ----------------------------
DROP TABLE IF EXISTS `submenu`;
CREATE TABLE `submenu` (
  `id_sub` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sub` varchar(50) NOT NULL,
  `mainmenu_idmenu` int(11) NOT NULL,
  `active_sub` varchar(20) NOT NULL,
  `icon_class` varchar(100) NOT NULL,
  `link_sub` varchar(50) NOT NULL,
  `sub_akses` varchar(12) NOT NULL,
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_sub`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of submenu
-- ----------------------------

-- ----------------------------
-- Table structure for tab_akses_mainmenu
-- ----------------------------
DROP TABLE IF EXISTS `tab_akses_mainmenu`;
CREATE TABLE `tab_akses_mainmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `c` int(11) DEFAULT '0',
  `r` int(11) DEFAULT '0',
  `u` int(11) DEFAULT '0',
  `d` int(11) DEFAULT '0',
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tab_akses_mainmenu
-- ----------------------------
INSERT INTO `tab_akses_mainmenu` VALUES ('1', '1', '1', null, '1', null, null, '2017-09-25 11:49:01', 'direktur');
INSERT INTO `tab_akses_mainmenu` VALUES ('2', '2', '1', '0', '1', '0', '0', '2018-09-24 10:18:10', '');
INSERT INTO `tab_akses_mainmenu` VALUES ('23', '3', '1', '0', '1', '0', '0', '2018-09-24 13:04:05', '');
INSERT INTO `tab_akses_mainmenu` VALUES ('24', '4', '1', '0', '1', '0', '0', '2018-09-24 15:27:24', '');
INSERT INTO `tab_akses_mainmenu` VALUES ('25', '5', '1', '0', '1', '0', '0', '2018-09-24 15:37:15', '');

-- ----------------------------
-- Table structure for tab_akses_submenu
-- ----------------------------
DROP TABLE IF EXISTS `tab_akses_submenu`;
CREATE TABLE `tab_akses_submenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sub_menu` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `c` int(11) DEFAULT '0',
  `r` int(11) DEFAULT '0',
  `u` int(11) DEFAULT '0',
  `d` int(11) DEFAULT '0',
  `entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_user` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tab_akses_submenu
-- ----------------------------

-- ----------------------------
-- Table structure for tb_about
-- ----------------------------
DROP TABLE IF EXISTS `tb_about`;
CREATE TABLE `tb_about` (
  `id_about` int(11) NOT NULL AUTO_INCREMENT,
  `about_logo` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `about_deskripsi` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `id_admin` int(11) DEFAULT NULL,
  `about_title_meta` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `about_deskripsi_meta` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `about_keyword_meta` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_about`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_about
-- ----------------------------
INSERT INTO `tb_about` VALUES ('1', 'logo2.png', 'MauGrafika adalah...', '1', '', '', '');

-- ----------------------------
-- Table structure for tb_foto
-- ----------------------------
DROP TABLE IF EXISTS `tb_foto`;
CREATE TABLE `tb_foto` (
  `id_gallery` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_nama` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `gallery_gambar` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `id_album` int(11) DEFAULT NULL,
  `gallery_title_meta` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `gallery_deskripsi_meta` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `gallery_keyword_meta` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_gallery`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_foto
-- ----------------------------
INSERT INTO `tb_foto` VALUES ('1', 'Paket', '1.jpg', null, '1', null, null, null);
INSERT INTO `tb_foto` VALUES ('2', 'Paket', '2.jpg', null, '1', null, null, null);
INSERT INTO `tb_foto` VALUES ('3', 'Paket', '3.jpg', null, '1', null, null, null);

-- ----------------------------
-- Table structure for tb_kontak
-- ----------------------------
DROP TABLE IF EXISTS `tb_kontak`;
CREATE TABLE `tb_kontak` (
  `id_kontak` int(11) NOT NULL AUTO_INCREMENT,
  `kontak_lat` varchar(100) DEFAULT NULL,
  `kontak_long` varchar(100) DEFAULT NULL,
  `kontak_deskripsi` text,
  `kontak_judul` varchar(30) DEFAULT NULL,
  `kontak_title_meta` varchar(200) DEFAULT NULL,
  `kontak_deskripsi_meta` text,
  `kontak_keyword_meta` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_kontak`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_kontak
-- ----------------------------
INSERT INTO `tb_kontak` VALUES ('1', '-7.9735745', '112.658746', 'Mau Grafika Terletak di ......', null, null, null, null);

-- ----------------------------
-- Table structure for t_detail_pengiriman
-- ----------------------------
DROP TABLE IF EXISTS `t_detail_pengiriman`;
CREATE TABLE `t_detail_pengiriman` (
  `id_detail_pengiriman` int(11) NOT NULL AUTO_INCREMENT,
  `id_detail_order` int(11) NOT NULL,
  `tanggal_konfirmasi` date NOT NULL,
  `tanggal_kirim` date DEFAULT NULL,
  `status_kadaluarsa` int(1) NOT NULL,
  PRIMARY KEY (`id_detail_pengiriman`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_detail_pengiriman
-- ----------------------------
INSERT INTO `t_detail_pengiriman` VALUES ('2', '2', '2017-01-31', '2017-01-31', '0');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `level` varchar(30) NOT NULL,
  `status` varchar(1) NOT NULL,
  `foto` text,
  `password` varchar(100) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `id_ongkir` int(11) NOT NULL,
  `aktif_user` tinyint(1) NOT NULL,
  `nama_rek_user` varchar(255) NOT NULL,
  `no_rek_user` varchar(255) NOT NULL,
  `bank_rek_user` varchar(255) NOT NULL,
  `view_password` varchar(100) DEFAULT NULL,
  `admin_level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('admin', 'admin', 'admin@admin.com', 'admin', '1', null, 'e00cf25ad42683b3df678c61f42c6bda', '', '', '0', '0', '', '', '', 'admin1', '1');
INSERT INTO `user` VALUES ('afif', 'wildan afif', 'wildan@gmail.com', 'member', '', null, 'e353cc5a41c4b79ffa728ce8b80d1b62', 'Jawa Timur', 'Malang', '256', '1', 'wildan afif', '3244325423', 'Mandiri', null, null);
INSERT INTO `user` VALUES ('AMurleloli', 'AMurleloli', 'quonahquee@bestmailonline.com', 'member', '', null, '68856c90365e35e3e8df64cc4e9c3345', '', '', '0', '0', 'AMurleloli', 'AMurleloli', 'La Strada  hardcore', null, null);
INSERT INTO `user` VALUES ('Briandab', 'Briandab', 'xrumm88965@gmail.com', 'member', '', null, '63710fe5626abcda4dc059508f4a7fd1', '', '', '0', '0', 'Briandab', 'Briandab', 'Pearl Jam', null, null);
INSERT INTO `user` VALUES ('danaukerinciraya', 'Admin', 'admin@admin.com', 'admin', '1', '', '827ccb0eea8a706c4c34a16891f84e7b', '0', '0', '0', '0', '', '', '', null, null);
INSERT INTO `user` VALUES ('david', 'David', 'davis', 'member', '', null, '172522ec1028ab781d9dfd17eaca4427', 'Jawa Timur', 'Malang', '255', '1', 'David', '23402840', 'Mandiri', null, null);
INSERT INTO `user` VALUES ('edoedo', 'edoedoeo', 'edo_guitarman.4171@yahoo.com', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Barat', 'Bandung', '22', '1', '', '', '', null, null);
INSERT INTO `user` VALUES ('eka', 'eka ramdani', 'admin@admin.com', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', 'DKI Jakarta', 'Jakarta Pusat', '152', '1', 'Eka Ramdani', '12324', 'BRI', null, null);
INSERT INTO `user` VALUES ('Emma', 'Fathimatuz Zahra', 'emma_zahra@gmail.com', 'member', '', null, '972a9f0dc30d2d552063983763dab7d8', 'Jawa Timur', 'Malang', '256', '1', '', '', '', null, null);
INSERT INTO `user` VALUES ('Ontownnageenano', 'Ontownnageenano', 'cxufmhorrespesia@maldonadomail', 'member', '', null, '6d1324945fb216a6c6348a04abc2058f', '', '', '0', '0', 'Ontownnageenano', 'Ontownnageenano', '', null, null);
INSERT INTO `user` VALUES ('pisang', 'Pisang', 'pisang@gmail.com', 'member', '', null, '4dc2a159b17b4725943816b8ba6d7ff5', 'Jawa Timur', 'Malang', '255', '1', 'Pisang', '23432532523', 'Mandiri', null, null);
INSERT INTO `user` VALUES ('rhetech', 'Rheza Arief', 'rhetech@yahoo.co.uk', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Sidoarjo', '409', '1', 'rheza arief ', '712537512375133', 'BNI', null, null);
INSERT INTO `user` VALUES ('ridhorobby', 'RIdho', 'ridho.robby50@yahoo.com', 'member', '1', null, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Bondowoso', '86', '1', '', '', '', null, null);
INSERT INTO `user` VALUES ('robby1', 'Robby', 'roby@robyyahoo.com', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', '0', '0', '0', '0', '', '', '', null, null);
INSERT INTO `user` VALUES ('robby12', 'Robby Bibi', 'roby@robyyahoo.com', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Malang', '256', '1', '', '', '', null, null);
INSERT INTO `user` VALUES ('saya', 'coba', 'coba@gmail.com', 'member', '', null, '81dc9bdb52d04dc20036dbd8313ed055', 'Jawa Timur', 'Jember', '160', '1', 'COba', '11111111', 'BCA', null, null);
INSERT INTO `user` VALUES ('SMKN4Malang', 'SMKN4Malang', 'yayanraw@gmail.com', 'member', '', null, 'd8578edf8458ce06fbc5bb76a58c5ca4', 'Jawa Timur', 'Malang', '255', '1', 'Yayan Rahmat Wijaya', '021223224244', 'BRI', null, null);
INSERT INTO `user` VALUES ('test', 'test', 'ic_troumax@yahoo.com', 'admin', '1', '', '22476aa1575b6ef6032e4c2c2e3a7b25', '0', '0', '0', '0', '', '', '', null, null);
INSERT INTO `user` VALUES ('Usernam', 'Admin', 'admin@admin.com', 'member', '', null, 'e10adc3949ba59abbe56e057f20f883e', 'DI Yogyakarta', 'Bantul', '39', '0', 'nama pem', 'no 12312', 'Mandiri', null, null);
INSERT INTO `user` VALUES ('wildan', 'wildan afif', 'wildanafif00@gmail.com', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Malang', '255', '1', 'Wildan Afif', '34234233', 'BRI', null, null);
INSERT INTO `user` VALUES ('wildanafif', 'wildan afif a', 'wildanafif.id@gmail.com', 'member', '', null, '827ccb0eea8a706c4c34a16891f84e7b', 'Jawa Timur', 'Blitar', '74', '1', 'wildan', '43534534', 'Mandiri', null, null);
INSERT INTO `user` VALUES ('wld', 'wildan afif', 'wildanafif.id@gmail.com', 'member', '', null, 'e353cc5a41c4b79ffa728ce8b80d1b62', 'Jawa Timur', 'Lumajang', '243', '0', 'erda', '324232', 'Mandiri', null, null);
