<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('Mdl_order');
        $this->auth->restrict();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->library("session");
    }
    
    function index(){
       // $this->mdl_home->getsqurity();
        $data['view_file']    = "moduls/order";
        $this->load->view('admin_view',$data);
    }
    
    public function ajax_list() {
        $list = $this->Mdl_order->get_datatables();
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $produk) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $produk->id_order;
            $row[] = $produk->tgl_order;
            $row[] = $produk->total_order;
            $row[] = $produk->nama_order;
            $row[] = $produk->alamat_order;
            $row[] = $produk->tlp_order;
            $row[] = $produk->kode_pos_order;
            $row[] = $produk->kota_order;
            $row[] = $produk->status_order;
            $row[] = '
            <a href="javascript:void(0)" onclick="edit('."'".$produk->id_order."'".')">
                            <button class="btn btn-white btn-info btn-bold">
                                <i class="ace-icon fa fa-pencil bigger-120 blue"></i>
                            </button>
                        </a>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_REQUEST['draw'],
                        "recordsTotal" => $this->Mdl_order->count_all(),
                        "recordsFiltered" => $this->Mdl_order->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function ajax_edit_status($id) {
        $data = $this->Mdl_order->get_by_id($id);
     //  print_r($this->db->last_query());
        echo json_encode($data);
    }

    public function ajax_update_status() {
        $data = array(
                'status_order'              => $this->input->post('status_order'),
            );
            
      
        $this->Mdl_order->update_status($this->input->post('id_order'), $data);
        
        
        //print_r($this->db->last_query());
        echo json_encode(array("status" => TRUE));
    }
}   