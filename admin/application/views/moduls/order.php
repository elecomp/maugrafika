<?php $title = "<i class='fa fa-briefcase'></i>&nbsp;Order"; ?>
<div id="idImgLoader" style="margin: 0 auto; text-align: center;">
	<img src='<?php echo base_url();?>assets/img/loader-dark.gif' />
</div>
<div id="data" style="display:none;">
<section class="content">
<div class="page-header">
	<h1>
		<?php echo $title;?>
	</h1>
</div><!-- /.page-header -->

<div id="panel-data">
<div class="widget-box">
<div class="widget-header">

	<div class="widget-toolbar">
		<a href="#" data-action="collapse">
			<i class="ace-icon fa fa-chevron-up"></i>
		</a>

		<a href="#" data-action="close">
			<i class="ace-icon fa fa-times"></i>
		</a>
	</div>
	</div>

<div class="widget-body">
<div class="widget-main">
<div class="row">
<div class="col-xs-12">
<div class="box-header">
	<button class="btn btn-default" onclick="reload_table()"><i class="fa fa-refresh"></i> Reload</button>

</div><br />
<table id="dynamic-table" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>No.</th>
            <th>Id Order</th>
            <th>Tanggal Order</th>
            <th>Total Order</th>
            <th>Nama Order</th>
            <th>Alamat Order</th>
            <th>Telp Order</th>
            <th>Kode Pos Order</th>
            <th>Kota Order</th>
            <th>Status Order</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
</div><!-- /.span -->
</div>					
</div><!-- /.row -->
</div>
</div>
</div>

<script>
	var save_method;
	var link = "<?php echo site_url('Order')?>";
	var table;
	$(document).ready(function(){
      //$('#idImgLoader').show(2000);
	  $('#idImgLoader').fadeOut(2000);
	  setTimeout(function(){
            data();
      }, 2000);
    });

    function data(){
		$('#data').fadeIn();
	}
	
	$(document).ready(function() {
		table = $('#dynamic-table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
		"bDestroy": true,
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Order/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

    });
	
	}).fnDestroy();
	
	function reload_table() {
    	table.ajax.reload(null, false);
	}

    function edit(id) {
        
        save_method = 'update';
        $('#form')[0].reset();

        $.ajax({
            url : "<?php echo site_url('Order/ajax_edit_status')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(result) {
                console.log(result);
                // document.getElementById('id_order').value = result.id_order;
                $('[name="id_order"]').val(result.id_order);
                // document.getElementById('form').innerHTML =' <input type="text" value="bar" id="id_order1" name="id_order1"> ';
                $('[name="status_order"]').val(result.status_order);
                $('#modal_form').modal('show');
            }, error: function (jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function simpan() {
            $('#btn_save').text('Saving...');
            $('#btn_save').attr('disabled', true);

            var url;
            
            url = link+"/ajax_update_status";

            $.ajax({
                url: url,
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function(result) {
                    if (result.status) {
                        
                        setTimeout(function(){
                            $('#btn_close').click();
                        }, 1000);
                        
                        setTimeout(function(){
                            reload_table();
                        }, 1000);
                    }
                    setTimeout(function(){
                        $('#btn_save').text('Save');
                        $('#btn_save').attr('disabled', false);
                        document.getElementById('form').reset();
                    }, 1000);
                    swal_berhasil(); 
                    setTimeout(function(){
                            reload_table();
                    }, 1000);
                }, error: function(jqXHR, textStatus, errorThrown) {
                    // alert('Error adding/update data');
                    swal({ title:"ERROR", text:"Error adding / update data", type: "warning", closeOnConfirm: true}); 
                    $('#btnSave').text('save'); $('#btnSave').attr('disabled',false);  
                }
            });
    }
</script>    

<div id="modal_form" class="modal fade" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header no-padding">
                                    <div class="table-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        <span class="white">&times;</span>
                                    </button>
                                    Status Order
                                    </div>
                                </div>

                                <div class="modal-body no-padding">
                                <div align="center">
                                <form id="form" class="form-horizontal"><br />
                                <input type="hidden" value=""  name="id_order"> 
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">  Status Order </label>
                                        <div class="col-sm-6">
                                            <select name="status_order" id="status_order" class="form-control">
                                            <option>--Pilih Status Order--</option>
                                            <option value="Transaksi Belum Bayar">Transaksi Belum Bayar</option>
                                            <option value="Menunggu Konfirmasi">Menunggu Konfirmasi</option>
                                            <option value="Sudah Bayar">Sudah Bayar</option>
                                            </select>
                                        </div>
                                </div><br />
                                <div class="form-group no-padding-right">
                                    <button class="btn btn-info" type="button" id="btn_save" onclick="simpan()">
                                        <i class="ace-icon fa fa-pencil bigger-110"></i>
                                        Ubah
                                    </button>
                                    <button type="button" id="btn_close" class="btn btn-default hide" data-dismiss="modal">Close</button>
                                </div>
                                </form>
                                </div>      
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
</div>  