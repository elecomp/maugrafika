<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Toko_online_model extends CI_Model {

    function page_query($table, $order_by, $limit, $offset) {
        $this->db->order_by($order_by, 'DESC');
        $this->db->limit($limit, $offset);
        $query = $this->db->get($table);
        if ($this->db->_error_message())
            header('Location: ../');
        return $query->result_array();
    }

    function get_table($table) {
        $query = $this->db->get($table);
        return $query->result_array();
    }

    function get_table_join_where($table1, $table2, $on, $where) {
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $on);
        $this->db->where($where);

        $query = $this->db->get();

        return $query->result_array();
    }

    function get_table_join3_where($table1, $table2, $table3, $on, $on2, $where) {
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $on);
        $this->db->join($table3, $on2);
        $this->db->where($where);

        $query = $this->db->get();

        return $query->result_array();
    }
    
    
    function get_table_joinnn_where($table1, $table2, $on, $where) {
        $this->db->select('*, SUM(detail_order.subtotal) AS jumlah');
        $this->db->from($table1);
        $this->db->join($table2, $on);
        $this->db->where($where);
        $this->db->group_by("order.id_order");
        $this->db->order_by("order.id_order", "DESC");

        $query = $this->db->get();

        return $query->result_array();
    }
    

    function get_table_joinn_where($table1, $table2, $on, $where) {
        $this->db->select('*, SUM(detail_order.subtotal) AS jumlah');
        $this->db->from($table1);
        $this->db->join($table2, $on);
        $this->db->where($where);
        $this->db->group_by("order.id_order");
        $this->db->order_by("order.id_order", "DESC");

        $query = $this->db->get();

        return $query->result_array();
    }

    function insert_table($table, $data) {
        $query = $this->db->insert($table, $data);
        return $query;
    }

    function get_table_where($table, $where) {
        $this->db->where($where);
        $query = $this->db->get($table);
        // if ($this->db->_error_message()) header('Location: ../');
        return $query->result_array();
    }

    function get_table_where_limit($table, $where,$limit) {
        $this->db->limit($limit);
        $this->db->where($where);
        $query = $this->db->get($table);
        // if ($this->db->_error_message()) header('Location: ../');
        return $query->result_array();
    }

    function update_table($table, $data, $where) {
        $this->db->where($where);
        $query = $this->db->update($table, $data);
    }

    function get_table_join_limit_order_by($from, $join, $where, $limit, $order_by) {
        $this->db->select('*');
        $this->db->from($from);
        $this->db->join($join, $where);
        $this->db->limit($limit);
        $this->db->order_by($order_by, 'DESC');
        $query = $this->db->get();
        if ($this->db->_error_message())
            header('Location: ../');
        return $query->result_array();
    }

    function get_special_limit($table, $start, $limit) {
        $this->db->limit($limit, $start);
        $query = $this->db->get($table);
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    function get_where_special_limit($table, $where, $start, $limit) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    function get_table_rows($table) {
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    function get_table_rows_where($row,$table,$where) {
        $this->db->select($row);
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }


    function get_produk($trigger) {
        if ($trigger == "terbaru") {
            $this->db->select('*');
            $this->db->from('produk');
            $this->db->where(array('validasi' => 1));
            $this->db->limit(12);
            $this->db->order_by('id_produk', 'DESC');
            $query = $this->db->get();
        } elseif ($trigger == "termurah") {
            $this->db->select('*');
            $this->db->from('produk');
            $this->db->where(array('validasi' => 1));
            $this->db->limit(8);
            $this->db->order_by('harga', 'ASC');
            $query = $this->db->get();
        } elseif($trigger == "semua"){
            $this->db->select('*');
            $this->db->from('produk');
            $this->db->where(array('validasi' => 1));
            $this->db->order_by('id_produk', 'DESC');
            $query = $this->db->get();
        } elseif($trigger == "promo"){
            // $this->db->select('*');
            // $this->db->from('produk');
            // $this->db->join('promo po','produk.id_produk = po.id_produk');
            // $this->db->where(array(
            //     'validasi' => 1,
            //     'po.tgl_promo' => date("d")));
            // $this->db->order_by('produk.id_produk', 'DESC');
            $select = 'SELECT *, IF ( SUBSTRING(po.tgl_promo, 3, 1) = "-", IF ( SUBSTRING(po.tgl_promo, 1, 2) <= date("d") && SUBSTRING(po.tgl_promo, 4, 2) >= date("d"), 1, 0 ), IF (po.tgl_promo = date("d"), 1, 0) ) AS coba FROM (`produk`) JOIN `promo` po ON `produk`.`id_produk` = `po`.`id_produk` WHERE `validasi` = 1  ORDER BY `produk`.`id_produk` DESC';
            $query = $this->db->query($select);
        }
        return $query->result_array();
    }

    function get_produk_form_id($id_kategori_produk) {
        $this->db->select('*');
        $this->db->from('produk');
        $this->db->where(array('kategori_produk' => $id_kategori_produk, 'validasi' => 1));
        $this->db->order_by('id_produk','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_keranjang_belanja($where) {
        $this->db->select("keranjang_belanja.*");
        $this->db->select("produk.*");
        // $this->db->select("produk.harga as harga_produk");
        // $this->db->select("produk.foto_produk1 as foto_produk1");
        // $this->db->select("produk.jumlah_stok as jumlah_stok");
        $this->db->from("keranjang_belanja")->join("produk", "keranjang_belanja.id_produk=produk.id_produk");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_penjual_cart($where) {
        $this->db->select("produk.id_user");
        // $this->db->select("produk.harga as harga_produk");
        // $this->db->select("produk.foto_produk1 as foto_produk1");
        // $this->db->select("produk.jumlah_stok as jumlah_stok");
        $this->db->from("keranjang_belanja")->join("produk", "keranjang_belanja.id_produk=produk.id_produk");
        $this->db->where($where);
        $this->db->group_by("produk.id_user");
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_order($where){
        $this->db->select("detail_order.*");
        $this->db->select("user.nama as nama");
        $this->db->select("produk.*");
        $this->db->from("detail_order")->join("user","detail_order.id_penjual=user.id_user");
        $this->db->join('produk', 'detail_order.id_produk = produk.id_produk');
        $this->db->where($where);
        $this->db->order_by("detail_order.id_penjual", "desc"); 
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_penjual_order($where){
        $this->db->select("id_penjual");
        $this->db->from("detail_order");
        $this->db->where($where);
        $this->db->order_by("id_penjual", "desc");
        $this->db->group_by("id_penjual"); 
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_penjual($where){
        $this->db->select("user.*");
        $this->db->from("produk")->join("user","produk.id_user=user.id_user");
        $this->db->where($where);
        $this->db->order_by("produk.id_user", "desc");
        $this->db->group_by("produk.id_user"); 
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_produk_penjual_cart($where){
        $this->db->select("keranjang_belanja.*");
        $this->db->select("produk.*");
        $this->db->from("keranjang_belanja")->join("produk","keranjang_belanja.id_produk=produk.id_produk");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_data_keranjang($where){
        $this->db->select("keranjang_belanja.*");
        $this->db->select("produk.*");
        $this->db->from("keranjang_belanja")->join("produk","keranjang_belanja.id_produk=produk.id_produk");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }

    function delete_table($table, $where) {
        $this->db->where($where);
        $query = $this->db->delete($table);
        return $query;
    }

    function select_table_order_limit($row, $namatabel, $order_trigger, $limit){
            $this->db->select($row);
            $this->db->from($namatabel);
            $this->db->order_by($order_trigger,"desc");
            $this->db->limit($limit); 
            $query = $this->db->get();
            return $query->result_array();

    }
    function get_count_result_where($id_kategori_produk,$table){
        $this->db->where(array('kategori_produk' => $id_kategori_produk, 'validasi' => 1));
        $this->db->from($table);

        return  $this->db->count_all_results();
    }

    function get_where_limmi($id_kategori_produk,$limit,$offset){
        $this->db->select('*');
        $this->db->from('produk');
        $this->db->where(array('kategori_produk' => $id_kategori_produk, 'validasi' => 1));
        $this->db->order_by('id_produk','DESC');
        $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function tentang(){
        return $this->db->get('tb_about');
    }

    public function foto(){
        return $this->db->get('tb_foto');
    }

    public function kontak(){
        return $this->db->get('tb_kontak');
    }

}
