<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo $title;?></title>
  <meta name="description" content="<?php echo $description;?>" />
    <meta name="keywords" content="<?php echo $keyword;?>" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Course Project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/'; ?>styles/bootstrap4/bootstrap.min.css">
<link href="<?php echo base_url().'assets/'; ?>plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/'; ?>plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/'; ?>plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/'; ?>plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/'; ?>styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/'; ?>styles/responsive.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/'; ?>styles/news_post_styles.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/'; ?>styles/news_post_responsive.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/'; ?>css/jquery.flipster.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/'; ?>styles/contact_styles.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/'; ?>styles/contact_responsive.css">

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<body>
