<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Beranda</a></li>
				  <li class="active">Konfirmasi Pembayaran</li>
				</ol>
			</div><!--/breadcrums-->
		</div>
</section>			

<section id="cart_items">
<div class="container">
	<table class="table table-condensed total-result">
									<tbody><tr>
										<td>ID Order</td>
										<td><?php echo $id_order ?></td>
									</tr>
									<tr>
										<td>Nama</td>
										<td><?php echo $order[0]['nama_order']; ?></td>
									</tr>
									<tr class="shipping-cost">
										<td>Kota</td>
										<td><?php echo $order[0]['alamat_order']; ?></td>										
									</tr>
									<tr>
										<td>Alamat</td>
										<td><span><?php echo $order[0]['kota_order']; ?> - <?php echo $order[0]['provinsi_order']; ?>, <?php echo $order[0]['kode_pos_order']; ?></span></td>
									</tr>
									<tr class="shipping-cost">
										<td>No Telp</td>
										<td><?php echo $order[0]['tlp_order']; ?></td>										
									</tr>
									<tr class="shipping-cost">
										<td>Email</td>
										<td><?php echo $order[0]['email_order']; ?></td>										
									</tr>
								</tbody>
	</table>

	<p><b>Berikut merupakan detail order<b></p>

<div class="panel panel-warning">
<div class="panel-heading"></div>
<div class="panel-body">
<table class="table table-striped">
  <thead>
      <tr>
        <th>Nama Produk</th>
        <th>Jumlah Produk</th>
        <th>Subtotal</th>
      </tr>
    </thead>
    <tbody>
    
    <?php 
                      foreach($detail_order as $key){
                    
                     ?>
                      <tr>
                        <td><strong><?php echo $key['nama_produk'] ?></strong></td>
                        <td><?php echo $key['jumlah_produk'] ?></td>
                        <td><?php echo $key['subtotal'] ?></td>
                      </tr>
                      
                      <?php } ?>



                      <tr>
                      <td>&nbsp;</th>
                      <td><b>Total Order Produk<b></td>
                      <td ><?php echo $order[0]['total_order']; ?></td>
                    </tr>
                     <tr>
                      <td>&nbsp;</th>
                      <td><b>Jumlah Tagihan<b></td>
                      <td ><?php echo $order[0]['grand_total_order']; ?></td>
                    </tr>
              
     
    </tbody>
</table>
</div>
</div>

<h3> Silahkan Melakukan pembayaran ke salah satu  rekening yang tersedia berikut ini:</h3>
<div class="panel panel-warning">
<div class="panel-heading"></div>
<div class="panel-body">
	  <table class="table">
    <thead>
      <tr>
        <th>Jenis Bank</th>
        <th>No Rekening</th>
        <th>Atas nama</th>
      </tr>
    </thead>
    <tbody>
    
    <?php 
                    	foreach($bank as $key){
                    
                     ?>
                      <tr>
                        <td><strong><?php echo $key['jenis_bank'] ?></strong></td>
                        <td><?php echo $key['no_rekening'] ?></td>
                        <td><?php echo $key['atas_nama_bank'] ?></td>
                      </tr>
                      
                      <?php } ?>
     
    </tbody>
  </table>
</div>
</div>
 <p  ><span style="color: red">*</span> Setelah Melakukan pembayaran silahkan konfirmasikan ke menu konfirmasi order</p>
</div>	
</section>

<script src="<?php echo base_url('assets/pdfjs/dist/jspdf.debug.js') ?>"></script>
  <script src="<?php echo base_url('assets/pdfjs/libs/html2pdf.js') ?>"></script>
  <script>


var doc = new jsPDF();
var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};


    doc.fromHTML($('#content').html(), 15, 15);
    doc.save('MauGrafika-<?php echo $id_order ?>.pdf');






    
        var pdf = new jsPDF('p', 'pt', 'letter');

        pageHeight= pdf.internal.pageSize.height;

        // Before adding new content
        y = 500 // Height position of new content
        if (y >= pageHeight)
        {
          pdf.addPage();
          y = 0 // Restart height position
        }
      

        
        var canvas = pdf.canvas;
   
        // var width = 400;
        html2pdf(document.getElementById("print"), pdf, function(pdf) {
              
                pdf.save('Test.pdf');

               //var div = pdfument.createElement('pre');
               //div.innerText=pdf.output();
               //pdfument.body.appendChild(div);
            }
        );


        //window.location = "http://www.facebook.com";
    </script>
