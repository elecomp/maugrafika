<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Beranda</a></li>
				  <li class="active">Kontak</li>
				</ol>
			</div><!--/breadcrums-->
		</div>
</section>	

<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-details">
						<div class="col-sm-5">
							<div class="view-product">
								<div id="peta" style="width:472px;height:295px;"></div>  
							</div>
						</div>
						<div class="col-sm-7">
							  <p><?php echo $kontak->kontak_deskripsi?></p>
						</div>	
					</div>
				</div>	
			</div>
		</div>
</section>	

 <script     src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFTimIhQoFCg8bF7PAMgDWi38QqqvaCx8">
  </script>
    <script type="text/javascript">
        (function() {
        window.onload = function() {
        var map;
        //Parameter Google maps
        var options = {
        zoom: 16, //level zoom
        //posisi tengah peta
        center:new google.maps.LatLng('<?php echo @$kontak->kontak_lat;?>' ,'<?php echo @$kontak->kontak_long?>'),
        mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        // Buat peta di
        var map = new google.maps.Map(document.getElementById('peta'), options);
        // Tambahkan Marker
        var locations = [
            ['<?php echo @$kontak->kontak_judul?>', '<?php echo @$kontak->kontak_lat;?>' ,'<?php echo @$kontak->kontak_long?>'],
        ];
        var infowindow = new google.maps.InfoWindow();

        var marker, i;
        /* kode untuk menampilkan banyak marker */
        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
        });
        /* menambahkan event clik untuk menampikan
        infowindows dengan isi sesuai denga
        marker yang di klik */

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
        infowindow.setContent(locations[i][0]);
        infowindow.open(map, marker);
        }
        })(marker, i));
        }


        };
        })();



</script>   		