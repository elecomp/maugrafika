<style>
div.gallery {
    border: 1px solid #ccc;
}

div.gallery:hover {
    border: 1px solid #777;
}

div.gallery img {
    width: 100%;
    height: auto;
}

div.desc {
    padding: 15px;
    text-align: center;
}

* {
    box-sizing: border-box;
}

.responsive {
    padding: 0 6px;
    float: left;
    width: 24.99999%;
}

@media only screen and (max-width: 700px) {
    .responsive {
        width: 49.99999%;
        margin: 6px 0;
    }
}

@media only screen and (max-width: 500px) {
    .responsive {
        width: 100%;
    }
}

.clearfix:after {
    content: "";
    display: table;
    clear: both;
}
</style>
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Beranda</a></li>
				  <li class="active">Foto</li>
				</ol>
			</div><!--/breadcrums-->
		</div>
</section>	
<section id="cart_items">
	<div class="container">
	  <div class="row">
	  
          <?php foreach ($foto as $ga) { ?>
          <div class="responsive">
			  <div class="gallery">
			    <a target="_blank" href="<?php echo base_url().'assets/images/'.$ga['gallery_gambar'] ?>">
			      <img src="<?php echo base_url().'assets/images/'.$ga['gallery_gambar'] ?>" alt="Cinque Terre" width="600" height="400">
			    </a>
			    <div class="desc"><a class="btn btn-fefault cart"><?php echo $ga['gallery_nama'] ?></a></div>
			  </div>
		 </div>
		 <?php } ?> 

	  </div>
	</div>
</section>	  <br /><br />