<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Beranda</a></li>
				  <li class="active">Tentang</li>
				</ol>
			</div><!--/breadcrums-->
		</div>
</section>	

<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="product-details">
						<div class="col-sm-5">
							<div class="view-product">
								<img src="<?php echo base_url() ?>assets/images/<?php echo $tentang->about_logo;?>" alt="" style="height: 1%;"/>
							</div>
						</div>
						<div class="col-sm-7">
							<?php echo $tentang->about_deskripsi;?>
						</div>	
					</div>
				</div>	
			</div>
		</div>
</section>			