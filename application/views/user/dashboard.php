<?php 
  $menu = $this->toko_online_model->get_table_where('menu', array('aktif_menu' => 1));
  $cart = $this->toko_online_model-> get_keranjang_belanja(array('keranjang_belanja.id_keranjang_belanja'=> $_SERVER['REMOTE_ADDR']));

  $quantity = 0;
  foreach ($cart as $c) {
    $quantity = $quantity + $c['jumlah_produk'];
  }
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MauGrafika.com SMK Negeri 4 Malang </title>
    <link href="<?php echo base_url().'assets/'; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/'; ?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/'; ?>css/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/'; ?>css/price-range.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/'; ?>css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/'; ?>css/main.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/'; ?>css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="<?php echo base_url().'assets/'; ?>css/jquery.flipster.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?php echo base_url().'assets/'; ?>images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url().'assets/'; ?>images/ico/apple-touch-icon-57-precomposed.png">

    <script src="<?php echo base_url().'assets/'; ?>js/jquery.min.js"></script>
    <script src="<?php echo base_url().'assets/'; ?>js/jquery.flipster.min.js"></script>
</head><!--/head-->

<body>
    <header id="header"><!--header-->
       <div class="header_top"><!--header_top-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="contactinfo">
                            <ul class="nav nav-pills">
                                <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="social-icons pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="header-middle"><!--header-middle-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="logo pull-left">
                            <a href="index.html"><img src="<?php echo base_url().'assets/'; ?>images/logo2.png" alt="" /></a>
                        </div>
                        <div class="btn-group pull-right">
                           
                            
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="shop-menu pull-right">
                            <ul class="nav navbar-nav">
                               
                                <button data-toggle="modal" data-target="#myModal" type="button" class="btn btn-fefault cart">
                                        <i class="fa fa-shopping-cart"></i>
                                        SHOPPING CART (<?php echo $quantity ?>)
                                </button>
                                <a href="<?php echo base_url() ?>order/konfirmasi_pembayaran" class="btn btn-fefault cart">Konfirmasi Pembayaran</a>
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog modal-lg" style="width: 77%;">
                                    
                                      <!-- Modal content-->
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">SHOPPING CART</h4>
                                        </div>
                                        <div class="modal-body">
                                         <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu">
                            <td class="image">Item</td>
                            <td class="description">Nama Produk</td>
                            <td class="price">Harga</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $total = 0;
                        foreach ($cart as $c) {
                        $total = $total + $c['subtotal_belanja'];
                      ?>
                        <tr>
                            <td style="width: 35%">
                               <img src="<?php echo base_url() ?>assets/images/<?php echo $c['foto_produk1'] ?>" alt="" style="width: 35%" >
                            </td>
                            <td class="cart_description">
                                <h4><a href="#"><?php echo $c['nama_produk'] ?></a></h4>
                            </td>
                            <td class="cart_price">
                                 <p><?php echo $c['jumlah_produk'].'x'.$c['subtotal_belanja'] ?></p>
                            </td>
                            <td class="cart_delete">
                                <a href="<?php echo base_url() ?>home/remove_keranjang_belanja/<?php echo $c['id'] ?>" class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    <?php } ?>    
                    </tbody>
                </table>
                <section id="do_action">
                    <div class="total_area">
                        <ul>
                            <li style="width: 80%;/* margin: 3px; */padding: 14px;">Total <span><?php echo 'Rp.'.$total ?></span></li>
                        </ul>
                            <a href="<?php echo base_url() ?>home/checkout" class="btn btn-default check_out" href="">Check Out</a>
                    </div>
                </section>    
            </div>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>
                                      
                                    </div>
                                  </div> 
                            <li>
                             
                            </li>         
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-middle-->
    
        <div class="header-bottom"><!--header-bottom-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="mainmenu pull-left">
                            <ul class="nav navbar-nav collapse navbar-collapse">
                                <li><a href="<?php echo base_url('home') ?>">Beranda</a></li>
                                <li><a href="<?php echo base_url('home/tentang') ?>">Tentang Kami</a></li>
                               <li><a href="<?php echo base_url('produk') ?>">Produk</a></li>
                                <li><a href="<?php echo base_url('home/foto') ?>">Galeri</a></li>
                                <li><a href="<?php echo base_url('home/kontak') ?>">Kontak Kami</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="search_box pull-right">
                            <input type="text" placeholder="Search"/>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-bottom-->
    </header><!--/header-->
    
    <?php 
        $this->load->view($content);
    ?>
    
    <footer id="footer"><!--Footer-->
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="companyinfo">
                            <h2><span>e</span>-shopper</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="col-sm-3">
                            <div class="video-gallery text-center">
                                <a href="#">
                                    <div class="iframe-img">
                                        <img src="<?php echo base_url().'assets/'; ?>images/home/iframe1.png" alt="">
                                    </div>
                                    <div class="overlay-icon">
                                        <i class="fa fa-play-circle-o"></i>
                                    </div>
                                </a>
                                <p>Circle of Hands</p>
                                <h2>24 DEC 2014</h2>
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="video-gallery text-center">
                                <a href="#">
                                    <div class="iframe-img">
                                        <img src="<?php echo base_url().'assets/'; ?>images/home/iframe2.png" alt="">
                                    </div>
                                    <div class="overlay-icon">
                                        <i class="fa fa-play-circle-o"></i>
                                    </div>
                                </a>
                                <p>Circle of Hands</p>
                                <h2>24 DEC 2014</h2>
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="video-gallery text-center">
                                <a href="#">
                                    <div class="iframe-img">
                                        <img src="<?php echo base_url().'assets/'; ?>images/home/iframe3.png" alt="">
                                    </div>
                                    <div class="overlay-icon">
                                        <i class="fa fa-play-circle-o"></i>
                                    </div>
                                </a>
                                <p>Circle of Hands</p>
                                <h2>24 DEC 2014</h2>
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="video-gallery text-center">
                                <a href="#">
                                    <div class="iframe-img">
                                        <img src="<?php echo base_url().'assets/'; ?>images/home/iframe4.png" alt="">
                                    </div>
                                    <div class="overlay-icon">
                                        <i class="fa fa-play-circle-o"></i>
                                    </div>
                                </a>
                                <p>Circle of Hands</p>
                                <h2>24 DEC 2014</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="address">
                            <img src="<?php echo base_url().'assets/'; ?>images/home/map.png" alt="">
                            <p>505 S Atlantic Ave Virginia Beach, VA(Virginia)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="footer-widget">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="single-widget">
                            <h2>Service</h2>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#">Online Help</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Order Status</a></li>
                                <li><a href="#">Change Location</a></li>
                                <li><a href="#">FAQ’s</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="single-widget">
                            <h2>Quock Shop</h2>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#">T-Shirt</a></li>
                                <li><a href="#">Mens</a></li>
                                <li><a href="#">Womens</a></li>
                                <li><a href="#">Gift Cards</a></li>
                                <li><a href="#">Shoes</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="single-widget">
                            <h2>Policies</h2>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#">Terms of Use</a></li>
                                <li><a href="#">Privecy Policy</a></li>
                                <li><a href="#">Refund Policy</a></li>
                                <li><a href="#">Billing System</a></li>
                                <li><a href="#">Ticket System</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="single-widget">
                            <h2>About Shopper</h2>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#">Company Information</a></li>
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">Store Location</a></li>
                                <li><a href="#">Affillate Program</a></li>
                                <li><a href="#">Copyright</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <div class="single-widget">
                            <h2>About Shopper</h2>
                            <form action="#" class="searchform">
                                <input type="text" placeholder="Your email address">
                                <button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
                                <p>Get the most recent updates from <br>our site and be updated your self...</p>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <p class="pull-left">Copyright © 2013 E-SHOPPER Inc. All rights reserved.</p>
                    <p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
                </div>
            </div>
        </div>
        
    </footer>
    
     <script>
    <!--
        setInterval(function(){
            $(function(){ $(".flipster").flipster({ style: 'carousel', start: 0 }); });
        },3000);
    -->
    </script>

  
   
    <script src="<?php echo base_url().'assets/'; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url().'assets/'; ?>js/jquery.scrollUp.min.js"></script>
   <script src="<?php echo base_url().'assets/'; ?>js/price-range.js"></script> 
     <script src="<?php echo base_url().'assets/'; ?>js/jquery.prettyPhoto.js"></script> 
    <script src="<?php echo base_url().'assets/'; ?>js/main.js"></script> 

</body>
</html>