 <section id="slider"><!--slider-->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#slider-carousel" data-slide-to="1"></li>
                            <li data-target="#slider-carousel" data-slide-to="2"></li>
                        </ol>
                        
                        <div class="carousel-inner">
                       <?php $a=0;  $i=0; foreach ($data_slider as $key): $a++;  ?>

                       <?php if($i<3) {
                         if ($a==1) {
                                 echo '<div class="item active">';
                             }else{
                                echo '<div class="item">';
                         }?>
                                <div class="col-sm-6">
                                    <h2><?php echo $key['nama_produk']; ?></h2>
                                    <p><?php echo $key['deskripsi']; ?> </p>
                                    <a href="<?php echo base_url('user/home/produk_detail/'.$key['id_produk']); ?>" class="btn btn-default get">Pesan Sekarang</a>
                                </div>
                                <div class="col-sm-6">
                                    <img src="<?php echo base_url() ?>assets/images/<?php echo $key['foto_produk1'] ?>" class="girl img-responsive" alt="" />
                                </div>
                            </div>
                           
                         <?php } ?>
                         <?php endforeach ?>    
                        </div>
                        
                        <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                    
                </div>
            </div>
        </div>
    </section><!--/slider-->
    
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Harga</h2>
                        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                            
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#">Dibawah Rp.10.000,-</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#">Rp.10.000,- - Rp.50.000,-</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#">Rp.50.000,- - Rp.100.000,-</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#">Rp.100.000,- - Rp.200.000,-</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#">Rp.200.000,- Rp.300.000,-</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#">Diatas Rp.300.000,-</a></h4>
                                </div>
                            </div>
                        </div><!--/category-products-->
                    
                        
                        
                       
                        
                     
                    
                    </div>
                </div>
                
                <div class="col-sm-9 padding-right">
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">Terbaru</h2>
                        <?php foreach ($terbaru as $t) {
                        # code...
                        ?>
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="<?php echo base_url() ?>assets/images/<?php echo $t['foto_produk1'] ?>" alt="" />
                                            <h2>Rp.<?php echo $t['harga'] ?></h2>
                                            <p><?php echo $t['nama_produk'] ?></p>
                                            <form id="cart" method="POST" action="<?php echo base_url() ?>home/keranjang_belanja">
                                              <input type="hidden" name="harga" value="<?php echo $t['harga'] ?>">
                                              <input type="hidden" name="id_produk" value="<?php echo $t['id_produk'] ?>">
                                              <input type="hidden" name="ip_number" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
                                              <input type="hidden" name="quantity" value="1">
                                            <a href="<?php echo base_url() ?>user/home/produk_detail/<?php echo $t['id_produk'] ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail</a>
                                            </form>
                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content">
                                                <h2>Rp.<?php echo $t['harga'] ?></h2>
                                                <p><?php echo $t['nama_produk'] ?></p>
                                                <form id="cart" method="POST" action="<?php echo base_url() ?>home/keranjang_belanja">
                                                  <input type="hidden" name="harga" value="<?php echo $t['harga'] ?>">
                                                  <input type="hidden" name="id_produk" value="<?php echo $t['id_produk'] ?>">
                                                  <input type="hidden" name="ip_number" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
                                                  <input type="hidden" name="quantity" value="1">
                                                <a href="<?php echo base_url() ?>home/produk_detail/<?php echo $t['id_produk'] ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail</a>
                                                </form>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                     <?php } ?>   
                    </div><!--features_items-->
                    
                    <div class="recommended_items"><!--recommended_items-->
                        <h2 class="title text-center">Produk Promo</h2>
                        
                        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="item active">  
                                <?php foreach ($promo as $p) { 
                                    if ($p['coba']==1) {?> 
                                    <div class="col-sm-4">
                                        <div class="product-image-wrapper">
                                            <div class="single-products">
                                                <div class="productinfo text-center">
                                                    <img src="<?php echo base_url() ?>assets/images/<?php echo $p['foto_produk1'] ?>"" alt="" />
                                                    <h2><?php echo $p['harga_promo'] ?></h2>
                                                    <p>Easy Polo Black Edition</p>
                                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                <?php }} ?>
                                </div>
                            </div>
                             <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                              </a>
                              <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                              </a>          
                        </div>
                    </div><!--/recommended_items-->
                    
                
                    
                </div>

            </div>
        </div>
    </section>