<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Beranda</a></li>
				  <li class="active">Form Konfirmasi Pembayaran</li>
				  <li><a href="#">Id Order: <?php echo $data_order[0]['id_order'] ?></a></li>
				</ol>
			</div><!--/breadcrums-->
		</div>
</section>	

<section id="cart_items">
	<div class="container">
	  <div class="row">
        <div class="col-md-12">
        <div class="panel panel-warning">
			<div class="panel-heading">Informasi Pengiriman</div>
			 <div class="panel-body">
        	 <form action="">
               <div class="table-responsive">
                  <table class="table">
                   
                    <thead>
                      <tr>
                        <td><strong>Nama Penerima</strong></td>
                        <td><?php echo $data_order[0]['nama_order'] ?></td>
                      </tr>
                      <tr>
                        <td><strong>Alamat Penerima</strong></td>
                        <td><?php echo $data_order[0]['alamat_order'] ?></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td><?php echo $data_order[0]['kode_pos_order'] ?></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td><?php echo $data_order[0]['kota_order'] ?></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td><?php echo $data_order[0]['provinsi_order'] ?></td>
                      </tr>
                      <tr>
                        <td><strong>Telepon</strong></td>
                        <td><?php echo $data_order[0]['tlp_order'] ?></td>
                      </tr>
                      <tr>
                        <td><strong>Email</strong></td>
                        <td><?php echo $data_order[0]['email_order'] ?></td>
                      </tr>
                      <tr>
                        <td><strong>Status</strong></td>
                        <td><?php echo $status_order ?></td>
                      </tr>

                     
                    </thead>
                  
                  </table>
                </div>
             </form> 
             </div>
          </div> 


          <div class="panel panel-warning">
			<div class="panel-heading">Detail Barang</div>
			 <div class="panel-body">
        	 <div class="table-responsive">
             <?php 
             $angka = 0;
             // print_r($data_penjual);
             foreach ($data_penjual as $penjual): ?>
                <h3>Penjual:<?php echo $penjual[0]['nama'] ?></h3>
                 
                  <table class="table">
                    <thead>
                      <tr>
                       
                        <th></th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                <?php 
                $total_perpenjual = 0;
                foreach ($penjual as $produk): ?>
                    <tbody>
                    <tr>
                        <td><a href="#"><img src="<?php echo base_url() ?>assets/images/<?php echo $produk['foto_produk1'] ?>" alt="img" width="250" height="200"></a></td>
                        <td><?php echo $produk['nama_produk'] ?></td>
                        <td><?php echo $produk['harga'] ?></td>
                        <td><?php echo $produk['jumlah_produk'] ?></td>
                        <td><?php echo $produk['subtotal'] ?></td>

                       
                    </tr>
                    
                <?php 
                $total_perpenjual = $total_perpenjual + $produk['subtotal'];
                endforeach ?>
                    <tr>
                        <td><strong>Total</strong></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?php echo $total_perpenjual ?></td>
                    </tr>        
                      </tbody>
                </table>
                <?php if ($penjual[0]['status_kirim'] == 1): ?>
                    <h4><strong>No.Resi: <?php echo $penjual[0]['no_resi'] ?></strong></h4>
                <?php endif ?>
             <?php $angka++; endforeach ?>
             <br>
             <br>  
             <table class="table">
                  <thead>
                      <tr>
                        <th>Total</th>
                        <th><?php echo $data_order[0]['total_order'] ?></th>
                      </tr>
                      <tr>
                        <th>Grand Total</th>
                        <th><?php echo $data_order[0]['grand_total_order'] ?></th>
                      </tr>
                    </thead>
                </table>           
           </div>
             </div>
          </div>  

         <?php if($data_order[0]['status_order'] == 1){ ?>	  
        <div class="panel panel-warning">
			<div class="panel-heading">Form Konfirmasi Pembayaran</div>
			 <div class="panel-body">

			  <div class="container">
     		  <div class="row">	

			  <form action="<?php echo base_url() ?>order/form_konfirmasi_pembayaran" method="POST" enctype="multipart/form-data">
			  <input type="hidden" name="id_order" value="<?php echo $data_order[0]['id_order'] ?>">
			 	<div class="form-group">
			 			<div class="col-md-8">
                              <label for="email">Tanggal Transfer<span>*</span>:</label>
                              
                                <input class="form-control" type="date" name="tanggal_transfer" placeholder="Tanggal Transfer*" required>
                              </div>  
                 </div> 
                 <div class="form-group">
			 			<div class="col-md-8">
                              <label for="email">Nama Pemilik Rekening Transfer<span>*</span>:</label>
                              
                                <input class="form-control" type="text" name="nama_pemilik" placeholder="Nama Pemilik Rekening Transfer*" required>
                              </div>  
                 </div> 
                  <div class="form-group">
			 			<div class="col-md-8">
                              <label for="email">Nomor Rekening Transfer<span>*</span>:</label>
                              
                                <input class="form-control" type="text" name="no_rekening" placeholder="Nomor Rekening Transfer*" required>
                              </div>  
                 </div>
                 <div class="form-group">
			 			<div class="col-md-8">
                              <label for="email">Bank Transfer<span>*</span>:</label>
                              
                                <input class="form-control" type="text" name="bank" placeholder="Bank Transfer*" required>
                              </div>  
                 </div> 
                 <div class="form-group">
			 			<div class="col-md-8">
                              <label for="email">Total Transfer<span>*</span>:</label>
                              
                                <input class="form-control" type="text" name="total_transfer" placeholder="Total Transfer" required>
                              </div>  
                 </div> 
                 <div class="form-group">
			 			<div class="col-md-8">
                              <label for="email">Foto Bukti Transfer<span>*</span>:</label>
                              
                                <input class="form-control" type="file" name="userfile" placeholder="Total Transfer" required>
                              </div>  
                 </div>
                 <div class="form-group">
                 	<div class="col-md-8">
			 			 <br /><br /><input class="btn btn-fefault cart" type="submit" value="Konfirmasi">
			 			</div>
			 	 </div>		
              </form>
              </div>
              </div>

			 </div>
		</div>	 
		 <?php } ?> 

        </div>
      </div>
    </div>
</section>        