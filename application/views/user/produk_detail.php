<section>
		<div class="container">
			<div class="row">
			
				
				<div class="col-sm-12">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<img src="<?php echo base_url() ?>assets/images/<?php echo $produk_detail[0]['foto_produk1'] ?>" alt="" />
								<h3><?php echo $produk_detail[0]['nama_produk'] ?></h3>
							</div>
							<div id="similar-product" class="carousel slide" data-ride="carousel">
								
								  <!-- Wrapper for slides -->
								    <div class="carousel-inner">
										<div class="item active">
										  <?php if($produk_detail[0]['foto_produk1']!="default.PNG"){ ?>
                      
				                      	 <a href="">
				                            <img src="<?php echo base_url() ?>assets/images/<?php echo $produk_detail[0]['foto_produk1'] ?>" width="85" height="84" >
				                          </a>     
				                          
				                          <?php } ?>    
				                          
				                           <?php if($produk_detail[0]['foto_produk2']!="default.PNG"){ ?>                           
				                          <a href="">
				                            <img src="<?php echo base_url() ?>assets/images/<?php echo $produk_detail[0]['foto_produk2'] ?>" width="85" height="84" >
				                          </a>
				                          <?php } ?>
				                          
				                           <?php if($produk_detail[0]['foto_produk3']!="default.PNG"){ ?>
				                           <a href="">
				                            <img src="<?php echo base_url() ?>assets/images/<?php echo $produk_detail[0]['foto_produk3'] ?>" width="85" height="84">
				                           </a>
				                          <?php } ?>
										</div>
										
									</div>

								  <!-- Controls -->
								  <a class="left item-control" href="#similar-product" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								  </a>
								  <a class="right item-control" href="#similar-product" data-slide="next">
									<i class="fa fa-angle-right"></i>
								  </a>
							</div>

						</div>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<h2><?php echo $produk_detail[0]['nama_produk'] ?></h2>
								<span>
									<span>Rp.<?php echo $produk_detail[0]['harga'] ?></span>
									<label>Quantity:</label>
									<form method="POST" action="<?php echo base_url() ?>home/keranjang_belanja">
                       				 <select id="" name="quantity">
			                          <?php 
			                            for ($i=1; $i <= $produk_detail[0]['jumlah_stok'] ; $i++) { 
			                              if($i == 1){
			                             ?>
			                              <option selected value="<?php echo $i ?>"><?php echo $i ?></option>
			                             <?php
			                             }
			                             else{
			                              ?>
			                               <option value="<?php echo $i ?>"><?php echo $i ?></option>
			                              <?php
			                             }
			                            }
			                           ?>
			                        </select>
			                          <input type="hidden" name="harga" value="<?php echo $produk_detail[0]['harga'] ?>">
				                      <input type="hidden" name="id_produk" value="<?php echo $produk_detail[0]['id_produk'] ?>">
				                      <input type="hidden" name="ip_number" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>"><br /><br />
									<button type="submit" class="btn btn-fefault cart">
										<i class="fa fa-shopping-cart"></i>
										Add to cart
									</button>
									</form>
								</span>
								<p ><b>Avilability:</b> <span><?php echo $produk_detail[0]['stok_produk'] ?></span></p>
								<p><b>Deskripsi:</b><?php echo $produk_detail[0]['deskripsi'] ?></p>
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					
					
					<div class="recommended_items"><!--recommended_items-->
						<h2 class="title text-center">Latest Produk</h2>
						
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								
			                  <div class="item active">
			                   <?php
			                  $i=1;
			                  $a=0;  
			                 foreach ($terbaru as $t ) { $a++; if ($i<5) {
			                  
			                  ?> 
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="<?php echo base_url() ?>assets/images/<?php echo $t['foto_produk1'] ?>" alt="" />
													<h2>Rp.<?php echo $t['harga'] ?></h2>
													<p><?php echo $t['nama_produk'] ?></p>
													 <form id="cart" method="POST" action="<?php echo base_url() ?>user/home/keranjang_belanja">
						                              <input type="hidden" name="harga" value="<?php echo $t['harga'] ?>">
						                              <input type="hidden" name="id_produk" value="<?php echo $t['id_produk'] ?>">
						                              <input type="hidden" name="ip_number" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
						                              <input type="hidden" name="quantity" value="1">
													<a href="<?php echo base_url() ?>user/home/produk_detail/<?php echo $t['id_produk'] ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Detail</a>
													</form>
												</div>
											</div>
										</div>
									</div>
								<?php } $i++;} ?>	
								
								</div>
							   
							</div>
							 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>			
						</div>
					</div><!--/recommended_items-->
					
				</div>
			</div>
		</div>
	</section>
