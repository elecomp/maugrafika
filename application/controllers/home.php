<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		 $this->load->model('toko_online_model');
		$this->load->library('session');
	}

	public function index()
	{	
		$data['terbaru'] = $this->toko_online_model->get_produk('terbaru');
	 
	 	$data['data_slider']	= $this->toko_online_model->get_produk('terbaru');
	 	$data['promo']	= $this->toko_online_model->get_produk('promo');
	 	// echo $this->db->last_query().'<br>';
		$data['content'] = 'user/index';
		$this->load->view('user/dashboard' , $data);
		
		
	}

	public function produk_detail($id_produk)
	{	
		// $data['menu'] =$this->toko_online_model->get_table_where('menu', array('aktif_menu' => 1));
		$data['terbaru'] = $this->toko_online_model->get_produk('terbaru');
		$data['produk_detail'] = $this->toko_online_model->get_table_where('produk', array('id_produk' => $id_produk));
		$data['content'] = 'user/produk_detail';
		$this->load->view('user/dashboard',$data);
	}

	public function keranjang_belanja(){
		$ip = $this->input->post('ip_number');
		$id_produk = $this->input->post('id_produk');
		$harga = $this->input->post('harga');
		$quantity = $this->input->post('quantity');
		$subtotal = $harga * $quantity;

		// echo $ip;

		$ambil_data = $this->toko_online_model->get_table_where('keranjang_belanja', array('id_keranjang_belanja' => $ip , 'id_produk' => $id_produk));

		

		
		if($ambil_data != null){
			$quantity_new = $ambil_data[0]['jumlah_produk'] + $quantity;
			$subtotal_new = $ambil_data[0]['subtotal_belanja'] + $subtotal;

			$data = array(
					'jumlah_produk' => $quantity_new,
					'subtotal_belanja' => $subtotal_new
				);

			$update = $this->toko_online_model->update_table('keranjang_belanja',$data, array('id_keranjang_belanja' => $ip , 'id_produk' => $id_produk));
		}
		else{
			$data = array(
			'id_keranjang_belanja' 	=> $ip,
			'id_produk'				=> $id_produk,
			'harga_produk'			=> $harga,
			'jumlah_produk'			=> $quantity,
			'subtotal_belanja'		=> $subtotal
			);
			$insert = $this->toko_online_model->insert_table('keranjang_belanja',$data);
		}

		redirect(base_url('/home/produk_detail/'.$id_produk));
		
	}

	public function remove_keranjang_belanja($id){
		$where = array(
			'id' 	=> $id
			);
		$this->toko_online_model->delete_table('keranjang_belanja', $where);
		//print_r($this->db->last_query());
		echo "<script>
				window.history.go(-1);
				location.reload();
		</script>";	
	}

	public function checkout(){
		$data['cart'] = $this->toko_online_model-> get_keranjang_belanja(array('keranjang_belanja.id_keranjang_belanja'=> $_SERVER['REMOTE_ADDR']));
		$penjual = $this->toko_online_model->get_penjual_cart(array('keranjang_belanja.id_keranjang_belanja'=> $_SERVER['REMOTE_ADDR']));
		$angka = 0;
		foreach ($penjual as $p) {
			$data_penjual[$angka] = $this->toko_online_model->get_table_where('user', array('id_user' => $p['id_user']));
			$angka++;
		}
		
		// $data_penjual = array();
		// $angka = 0;
		// foreach ($data['cart'] as $cart) {
		// 	$data_penjual[$angka] = $this->toko_online_model->get_penjual(array('produk.id_produk' => $cart['id_produk']));
		// 	// print_r($data_penjual[$angka]);
		// 	// echo "<br>";
		// 	$angka++;
		// }
		// foreach ($data_penjual as $penjual) {
		// 	if
		// }
		
		$data['penjual'] = $data_penjual;
		
		$data['content'] = 'user/checkout';
		$this->load->view('user/dashboard',$data);
	}

	public function tentang(){
		$data['content'] = 'user/tentang';
		$data['tentang'] = $this->toko_online_model->tentang()->row();
		$this->load->view('user/dashboard',$data);
	}

	public function foto(){
		$data['content'] = 'user/foto';
		$data['foto'] = $this->toko_online_model->foto()->result_array();
		$this->load->view('user/dashboard',$data);
	}

	public function kontak(){
		$data['content'] = 'user/kontak';
		$data['kontak'] = $this->toko_online_model->kontak()->row();
		$this->load->view('user/dashboard',$data);
	}


}

/* End of file welcome.php */
